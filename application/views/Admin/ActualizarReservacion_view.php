<link href="http://code.jquery.com/ui/1.10.3/themes/pepper-grinder/jquery-ui.css" rel="stylesheet" />
<script src="http://code.jquery.com/jquery-1.9.1.js" type="text/javascript"></script>
<script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js" type="text/javascript"></script>
<script src="<?php echo base_url("assets/js/jquery.meio.mask.min.js")?>" type="text/javascript"></script>
<script type="text/javascript">
	$(document).ready(function(){
		$("#Estatus").val($("#EstatusActual").val());
		$("#Fecha_Reg").datepicker({ dateFormat: "yy-mm-dd" });
		
		$("#Estatus").on("change",function(){
			if($(this).val()==3){
				$("#Fecha_Reg").show();
				$("#LblFecha_Reg").hide();
			}
			else{
				$("#Fecha_Reg").hide();
				$("#LblFecha_Reg").show();
			}
		});
	});
</script>
<?php echo form_open("admin/ActualizarReservacion_POST") ?>
<table class="table formulario table-borded" style="width:900px;">
	<tr>
		<td class="txtNegritas">Nombre:</td>
		<td><?php echo $Reservacion[0]->Nombre?></td>
	</tr>
	<tr>
		<td class="txtNegritas">Apellidos:</td>
		<td><?php echo $Reservacion[0]->Apellidos?></td>
	</tr>
	<tr>
		<td class="txtNegritas">Telefono:</td>
		<td><?php echo $Reservacion[0]->Telefono?></td>
	</tr>
	<tr>
		<td class="txtNegritas">e-mail:</td>
		<td><?php echo $Reservacion[0]->Email?></td>
	</tr>
	<tr>
		<td class="txtNegritas">Fecha de reservación:</td>
		<td>
			<label id="LblFecha_Reg"><?php echo $Reservacion[0]->Fecha_Res?></label>
			<input name="Fecha_Reg" id="Fecha_Reg" readonly="readonly" value="<?php echo $Reservacion[0]->Fecha_Res?>" type="text" style="cursor:pointer; display:none; background:none; width:100px;" />
		</td>
	</tr>
	<tr>
		<td class="txtNegritas">Hora de reservación:</td>
		<td><?php echo $Reservacion[0]->Hora_Res?></td>
	</tr>
	<tr>
		<td class="txtNegritas">Número de personas:</td>
		<td><?php echo $Reservacion[0]->Num_Personas?></td>
	</tr>
	<tr>
		<td class="txtNegritas">Comentarios</td>
		<td><?php echo $Reservacion[0]->Comentarios?></td>
	</tr>
	<tr>
		<td class="txtNegritas">Estatus:</td>
		<td>
			<select name="Estatus" id="Estatus">
				<option value="1">PENDIENTE</option>
				<option value="2">RESERVACIÓN REALIZADA</option>
				<option value="3">POSPONER</option>
				<option value="4">CANCELAR</option>
			</select>
			<input type="hidden" value="<?php echo $Reservacion[0]->Estatus?>" id="EstatusActual" />
			<input type="hidden" value="<?php echo $Reservacion[0]->Id_Reservacion?>" name="Id_Reservacion" />
		</td>
	</tr>
	<tr>
		<td><input type="submit" value="Guardar" /></td>
		<td></td>
	</tr>
</table>
<?php echo form_close() ?>
