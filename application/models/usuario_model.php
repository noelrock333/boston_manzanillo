<?php
class Usuario_model extends CI_Model {
	function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }
	
	function ObtenerUsuario($Username, $Password){
		$this->load->database();	//Con esta funcion abrimos conexion con la base de datos
		$this->db->select('*');		//Seleccionamos los campos
		$this->db->from('usuario');	//Seleccionamos la tabla
		$this->db->where('Username',$Username);
		$this->db->where('Password',do_hash($Password, 'md5'));
		$query = $this->db->get();	//En esta linea se ejecuta la consulta
		$this->db->close();			//Con esta funcion cerramos la conexion con la base de datos
		
		if($query->num_rows()>0){
			$row = $query->result();
			$this->session->set_userdata('NombreCompleto', $row[0]->Nombre." ".$row[0]->Apellido_paterno." ".$row[0]->Apellido_materno);
			$this->session->set_userdata('Id_Usuario', $row[0]->Id_Usuario);
			return "true";
		}
		else{
			return "false";
		}
	}
		
	function Registro($Nombre, $Apellido_paterno, $Apellido_materno, $Username, $Password){
		$this->load->database();
		$this->db->select('*');		//Seleccionamos los campos
		$this->db->from('usuario');	//Seleccionamos la tabla
		$this->db->where('Username',$Username);
		$query = $this->db->get();	//En esta linea se ejecuta la consulta

		if($query->num_rows()>0){
			return "true";
		}
		else{
			$InsertUser = array(
			   'Nombre' => $Nombre ,
			   'Apellido_paterno' => $Apellido_paterno ,
			   'Apellido_materno' => $Apellido_materno,
			   'Username' => $Username ,
			   'Password' => $Password 
			);
			$this->db->insert('usuario', $InsertUser); 
			return "false";
		}
		
		
	}
	
}
?>
