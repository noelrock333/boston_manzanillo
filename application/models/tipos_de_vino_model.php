<?php

    class Tipos_de_vino_model extends CI_Model {

	
	function __construct() {
		 $this->load->database();
		 parent::__construct();
	}

	function Insertar($Descripcion,$Idioma){
		$Datos = array(
		'Descripcion'=>$Descripcion,
		'Idioma'=>$Idioma
		);
		$this->db->insert("tipos_de_vino",$Datos);
		$this->db->close();		
	}	

	function Actualizar($Id_Tipo_De_Vino,$Descripcion,$Idioma){
		$Datos = array(
			'Descripcion'=>$Descripcion,
			'Idioma'=>$Idioma
		);
		$this->db->where('Id_Tipo_De_Vino',$Id_Tipo_De_Vino);
		$this->db->update('tipos_de_vino',$Datos);
		$this->db->close();
	}
	
	function Mostrar($Id_Tipo_De_Vino){
		$this->db->select('*'); //regresa todos los campos
		$this->db->from('tipos_de_vino');
		$this->db->where('Id_Tipo_De_Vino',$Id_Tipo_De_Vino);
		$resultado = $this->db->get();
		$this->db->close();
		return $resultado->result(); // despues del return solo es codigo inaccesible
	}
	
	function Eliminar($Id_Tipo_De_Vino){		
		$this->db->where('Id_Tipo_De_Vino',$Id_Tipo_De_Vino);
		$this->db->delete('tipos_de_vino');
		$this->db->close();
	}
	
	Function MostrarPorIdioma($Idioma){
		$this->db->Select('*');
		$this->db->from('tipos_de_vino');
		if($Idioma != "TODOS"){
			$this->db->where('Idioma',$Idioma);
		}
		$resultado = $this->db->get();
		$this->db->close();
		return $resultado->result();
	}
}
?>