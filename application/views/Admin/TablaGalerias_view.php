<table class="table table-striped table-borded tblResultados" id="TBLGalerias">
	<thead>
		<tr>
			<th>Titulo</th>
			<th>Tapa</th>
			<th>Fecha</th>
			<th class="tdAcciones filter-false">Acciones</th>
		</tr>
	</thead>
<?php
if(isset($Galerias)){
	foreach ($Galerias as $row){
		echo '<tr>';
		echo '<td>'.$row->Titulo.'</td>';
		echo '<td>'.$row->URL_Tapa.'</td>';
		echo '<td>'.$row->Fecha.'</td>';
		echo '<td class="tdAcciones"><a href="'.$row->Id_Galeria.'" class="eliminar">Eliminar</a> <a href="'.site_url("admin/ActualizarGaleria")."/".$row->Id_Galeria.'">Modificar</a></td>';
		echo '</tr>';
	}
}
?>
</table>
<?php $this->load->view("Admin/TablePAGER_view") ?>