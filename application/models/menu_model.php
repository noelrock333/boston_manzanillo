<?php
/**
 * 
 */
class Menu_model extends CI_Model {

	
	function __construct() {
		 $this->load->database();
		 parent::__construct();
	}

	function Insertar($Nombre,$Descripcion,$Descripcion_Corta,$Imagen,$Tipo,$Idioma){
		$Datos = array(
		'Nombre'			=>$Nombre,
		'Descripcion'		=>$Descripcion,
		'Descripcion_Corta'	=>$Descripcion_Corta,
		'Imagen'			=>$Imagen,
		'Tipo'				=>$Tipo,
		'Idioma'			=>$Idioma
		);
		$this->db->insert("menu",$Datos);
		$this->db->close();		
	}	

	function Actualizar($Id_Alimento,$Nombre,$Descripcion,$Descripcion_Corta,$Imagen,$Tipo,$Idioma){
		$Datos = array(
			'Nombre'=>$Nombre,
			'Descripcion'=>$Descripcion,
			'Descripcion_Corta'=>$Descripcion_Corta,
			'Imagen'=>$Imagen,
			'Tipo'=>$Tipo,	
			'Idioma'=>$Idioma	
		);
		$this->db->where('Id_Alimento',$Id_Alimento);
		$this->db->update('menu',$Datos);
		$this->db->close();
	}
	
	function Mostrar($Id_Alimento){
		$this->db->select('*'); //regresa todos los campos
		$this->db->from('menu');
		$this->db->where('Id_Alimento',$Id_Alimento);
		$resultado = $this->db->get();
		$this->db->close();
		return $resultado->result(); // despues del return solo es codigo inaccesible
	}
	
	function Eliminar($Id_Alimento){		
		$this->db->where('Id_Alimento',$Id_Alimento);
		$this->db->delete('menu');
		$this->db->close();
	}
	
	function MostrarTipo($Tipo){
		$this->db->select('*');
		$this->db->from('menu');
		if ($Tipo != 0){
			$this->db->where('Tipo',$Tipo);
		}
		$resultado = $this->db->get();
		$this->db->close();
		return $resultado->result();
	}
	
	Function MostrarPorIdioma($Idioma){
		$this->db->Select('*');
		$this->db->from('menu');
		$this->db->where('Idioma',$Idioma);
		$resultado = $this->db->get();
		$this->db->close();
		return $resultado->result();
	}
	
}

?>
