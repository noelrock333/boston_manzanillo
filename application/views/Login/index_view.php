<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01//EN"
"http://www.w3.org/TR/html4/strict.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" lang="es">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<title>LOGIN</title>
		<link rel="stylesheet" href="<?php echo base_url('assets/css/adminLayout.css')?>" />
		<link rel="stylesheet" href="<?php echo base_url('assets/css/font-awesome.min.css')?>" />
	</head>
	<body>
		<div id="ToolBar">
			<center><h3 style='color:#FFF;'>Administrador de contenidos</h3></center>
		</div>
		<br />
		<?php echo form_open('login/IniciarSesion');?>
		<table class="table table-bordered" style="width: 220px; margin:auto;">
			<tr>
				<td align="center"><img src="<?php echo base_url('assets/images/logo.png')?>" /></td>
			</tr>
			<tr>
				<td><input type="text" name="Username" placeholder="Usuario"/></td>
			</tr>
			<tr>
				<td><input type="password" name="Password" placeholder="Contraseña"/></td>
			</tr>
			<tr>
				<td>
					<input type="submit" value="Iniciar sesión" class="btn btn-primary"/>
					
					<?php if(isset($resultado)){
					echo "<h4 class='text-error' style='display:inline;'>Datos erroneos</h4>";} ?>
			</td>
			</tr>
		</table>
	</body>
</html>

