<?php
class Imagenes_galeria_model extends CI_Model {

	function __construct() {
		 $this->load->database();
		 parent::__construct();
	}

	function Insertar($URL_Imagen,$URL_ImagenMin,$Id_Galeria){
		$Datos = array(
			'URL_Imagen'=>$URL_Imagen,
			'URL_ImagenMin'=>$URL_ImagenMin,
			'Id_Galeria'=>$Id_Galeria
		);
		$this->db->insert("imagenes_galeria",$Datos);
		
		if($this->db->affected_rows() > 0){
			return $this->db->insert_id();
		}
		else{
			$this->db->close();
			return 0;
		}
	}
	
	function ObtenerTodo($Id_Galeria){
		$this->db->select('*'); //regresa todos los campos
		$this->db->from('imagenes_galeria');
		$this->db->where('Id_Galeria',$Id_Galeria);
		$resultado = $this->db->get();
		$this->db->close();
		return $resultado->result(); 
	}
	
	function Eliminar($Id_Imagen){		
		$this->db->where('Id_Imagen',$Id_Imagen);
		$this->db->delete('imagenes_galeria');
		$this->db->close();
	}
	
	function EliminarPorGaleria($Id_Galeria){		
		$this->db->where('Id_Galeria',$Id_Galeria);
		$this->db->delete('imagenes_galeria');
		$this->db->close();
	}
}
?>