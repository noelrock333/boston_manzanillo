<script>
	$(document).ready(function(){
		$(".eliminar").click(function(event){
			event.preventDefault();
			if( confirm("¿Desea eliminar?") ){
				$.ajax({
					type: "GET",
					url: "<?php echo site_url("admin/EliminarNacionalidad") ?>/"+$(this).attr('href'),
					}).done(function() {
				});
				$(this).parent().parent().empty();
			}
		});
	});
</script>
<table class="table table-striped table-borded">
	<thead>
		<tr>
			<th>Nombre</th>
			<th>Icono</th>
			<th colspan="2">Acciones</th>
		</tr>
	</thead>
<?php
if(isset($Nacionalidades)){
	foreach ($Nacionalidades as $row){
		echo '<tr>';
		echo '<td>'.$row->Nombre.'</td>';
		echo '<td><img src="'.$row->Imagen.'" /></td>';
		echo '<td><a href="'.$row->Id_Nacionalidad.'" class="eliminar">Eliminar</a></td>';
		echo '<td><a href="'.site_url("admin/ActualizarNacionalidad")."/".$row->Id_Nacionalidad.'">Modificar</a></td>';
		echo '</tr>';
	}
}
?>
</table>