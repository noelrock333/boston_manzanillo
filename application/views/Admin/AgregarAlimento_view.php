<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.0/jquery.min.js"></script>
<script type="text/javascript" src="<?php echo base_url('AjaxFileUploader/ajaxfileupload.js') ?>"></script>
<script type="text/javascript">
function ajaxFileUpload()
{
	$("#loading")
	.ajaxStart(function(){
		$(this).show();
	})
	.ajaxComplete(function(){
		$(this).hide();
	});

	$.ajaxFileUpload({
			url:'<?php echo base_url('AjaxFileUploader/doajaxfileupload.php') ?>',
			secureuri:false,
			fileElementId:'fileToUpload',
			dataType: 'json',
			data:{name:'logan', id:'id'},
			success: function (data, status){
				if(typeof(data.error) != 'undefined'){
					if(data.error != ''){
						alert(data.error);
						$("#ImagenSubida").text("Sin Imagen").removeClass("ConImagen").addClass("SinImagen");
						$("#Imagen").val("");
					}
					else{
						$("#ImagenSubida").text("Con imagen").removeClass("SinImagen").addClass("ConImagen");
						$("#Imagen").val('<?php echo base_url('/assets/images/subidas')?>'+'/'+data.fname);
					}
				}
			},
			error: function (data, status, e){
				alert(e);
			}
	})
	return false;
}
</script>
<?php echo form_open('admin/AgregarAlimento_POST')?>
	<table class="formulario" style="width: 900px;">
		<tr>
			<td>
				<span>Idioma</span>
				<select name="Idioma" id="Idioma">
					<option value="ES">ESPAÑOL</option>
					<option value="EN">INGLÉS</option>
				</select>
			</td>
		</tr>
		<tr>
			<td>
				<span>Tipo</span>
				<select name="Tipo">
					<option value="0"></option>
					<option value="1">DESAYUNO</option>
					<option value="2">COMIDA/CENA</option>
					<option value="3">POSTRE</option>
					<option value="4">BEBIDA</option>
				</select>
			</td>
		</tr>
		<tr>
			<td>
				<label>Nombre</label>
				<input type="text" name="Nombre"/>
			</td>
		</tr>
		<tr>
			<td>
				<label>Imagen</label>
				<input id="fileToUpload" type="file" size="45" name="fileToUpload" class="input">
				<button class="button" id="buttonUpload" onclick="return ajaxFileUpload();">Subir</button>
				<input type="hidden" name="Imagen" id="Imagen"/>
				<span id="ImagenSubida" class="SinImagen">Sin imagen</span>
			</td>
		</tr>
		<tr>
			<td>
				<label>Descripción corta</label>
				<textarea type="text" name="Descripcion_Corta"></textarea>
			</td>
		</tr>
		<tr>
			<td>
				<label>Descripción</label>
				<textarea type="text" name="Descripcion"></textarea>
			</td>
		</tr>
		<tr>
			<td><input type="submit" value="Guardar"/></td>
		</tr>
	</table>
<?php echo form_close()?>