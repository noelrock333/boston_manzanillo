<table class="table table-striped table-borded tblResultados" id="TBLAlinetos">
	<thead>
		<tr>
			<th>Nombre</th>
			<th>Descripción Corta</th>
			<th>Imagen</th>
			<th class="tdAcciones filter-false">Acciones</th>
		</tr>
	</thead>
<?php
if(isset($Alimentos)){
	foreach ($Alimentos as $row){
		echo '<tr>';
		echo '<td>'.$row->Nombre.'</td>';
		echo '<td>'.substr(strip_tags($row->Descripcion_Corta),0,150).'</td>';
		echo '<td><img src="'.$row->Imagen.'" /></td>';
		echo '<td class="tdAcciones"><a href="'.$row->Id_Alimento.'" class="eliminar">Eliminar</a> <a href="'.site_url("admin/ActualizarAlimento")."/".$row->Id_Alimento.'">Modificar</a></td>';
		echo '</tr>';
	}
}
?>
</table>
<?php $this->load->view("Admin/TablePAGER_view") ?>