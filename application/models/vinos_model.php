<?php
	class Vinos_model extends CI_Model {

		function __construct() {
			 $this->load->database();
			 parent::__construct();
		}
		function Insertar($Precio,$Descripcion,$Idioma,$Id_Nacionalidad,$Id_Tipo_De_Vino){
			$Datos = array(
				'Precio'			=>$Precio,
				'Descripcion'		=>$Descripcion,
				'Idioma'			=>$Idioma,
				'Id_Nacionalidad' 	=>$Id_Nacionalidad,
				'Id_Tipo_De_Vino'	=>$Id_Tipo_De_Vino
			);
			$this->db->insert("vinos",$Datos);
			$this->db->close();		
		}	
	
		function Actualizar($Id_Vino,$Precio,$Descripcion,$Idioma,$Id_Nacionalidad,$Id_Tipo_De_Vino){
			$Datos = array(
				'Precio'			=>$Precio,
				'Descripcion'		=>$Descripcion,
				'Idioma'			=>$Idioma,
				'Id_Nacionalidad' 	=>$Id_Nacionalidad,
				'Id_Tipo_De_Vino'	=>$Id_Tipo_De_Vino
			);
			$this->db->where('Id_Vino',$Id_Vino);
			$this->db->update('vinos',$Datos);
			$this->db->close();
		}
		
		function Mostrar($Id_Vino){
			$this->db->select('*'); //regresa todos los campos
			$this->db->from('vinos');
			$this->db->where('Id_Vino',$Id_Vino);
			$resultado = $this->db->get();
			$this->db->close();
			return $resultado->result(); // despues del return todo es codigo inaccesible
		}
		
		function Eliminar($Id_Vino){		
			$this->db->where('Id_Vino',$Id_Vino);
			$this->db->delete('vinos');
			$this->db->close();
		}
		
		function MostrarPorIdioma($Idioma){
			$this->db->Select('*');
			$this->db->from('vinos');
			$this->db->where('Idioma',$Idioma);
			$resultado = $this->db->get();
			$this->db->close();
			return $resultado->result();
		}
		
		function MostrarPorIdiomaNacionTipo($Idioma,$Id_Nacionalidad,$Id_Tipo_De_Vino){
			$this->db->Select('*');
			$this->db->from('vinos');
			if($Idioma != "TODOS"){
				$this->db->where('Idioma',$Idioma);
			}
			if($Id_Nacionalidad != 0){
				$this->db->where('Id_Nacionalidad',$Id_Nacionalidad);
			}
			if($Id_Tipo_De_Vino != 0){
				$this->db->where('Id_Tipo_De_Vino',$Id_Tipo_De_Vino);
			}
			$resultado = $this->db->get();
			$this->db->close();
			return $resultado->result();
		}
		
		function MostrarOrdernado($Idioma){
			$this->db->select('*');
			$this->db->from('vinos');
			//$this->db->join('nacionalidades', 'vinos.Id_Nacionalidad = nacionalidades.Id_Nacionalidad');
			$this->db->where('vinos.Idioma',$Idioma);
			$this->db->order_by('vinos.Id_Nacionalidad', 'asc');
			$this->db->order_by('vinos.Id_Tipo_De_Vino', 'asc');
			$resultado = $this->db->get();
			$this->db->close();
			return $resultado->result();
		}
		
		function ContarNaciones($Idioma){
			$this->db->select('Id_Nacionalidad');
			$this->db->from('vinos');
			$this->db->where('Idioma',$Idioma);
			$this->db->group_by('Id_Nacionalidad');
			$resultado = $this->db->get()->result();
			$this->db->close();
			return $resultado;
		}
	}
?>