<table class="table table-striped table-borded tblResultados" id="TBLVinos">
	<thead>
		<tr>
			<th>Descripción</th>
			<th>Precio</th>
			<th>Idioma</th>
			<th class="tdAcciones filter-false">Acciones</th>
		</tr>
	</thead>
<?php
if(isset($Vinos)){
	foreach ($Vinos as $row){
		echo '<tr>';
		echo '<td>'.$row->Descripcion.'</td>';
		echo '<td>$'.$row->Precio.'</td>';
		echo '<td>'.$row->Idioma.'</td>';
		echo '<td class="tdAcciones"><a href="'.$row->Id_Vino.'" class="eliminar">Eliminar</a> <a href="'.site_url("admin/ActualizarVino")."/".$row->Id_Vino.'">Modificar</a></td>';
		echo '</tr>';
	}
}
?>
</table>
<div id="pager" class="tablesorterPager">
	<form>
		<table>
			<tr>
				<td>
					<i class="icon-step-backward first"></i>
					<i class="icon-backward prev"></i>
				<td>
					<input type="text" class="pagedisplay"/>
				</td>
				<td>
					<i class="icon-forward next"></i>
					<i class="icon-step-forward last"></i>
				</td>
			</tr>
			<tr>
				<td colspan="3">
					<select class="pagesize">
						<option selected="selected"  value="10">10</option>
						<option value="20">20</option>
						<option value="30">30</option>
						<option  value="40">40</option>
					</select>
				</td>
			</tr>
		</table>
		
	</form>
</div>