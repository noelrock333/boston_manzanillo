<?php
    class Admin extends CI_Controller 
    {
		public function __construct()
		{
			parent::__construct();
			if(!$this->session->userdata('Id_Usuario')){
				redirect("login/index");
			}
		}
		
		public function index(){
			$data['Vista'] = "Admin/index_view";
			$this->load->view('Admin/AdminLayout_view',$data);
		}
		
		/* Alimentos */
		
		public function AgregarAlimento(){
			$data['Vista'] = "Admin/AgregarAlimento_view";
			$this->load->view('Admin/AdminLayout_view',$data);
		}
		
		public function AgregarAlimento_POST(){
			$Tipo		= $this->input->post("Tipo");
			$Nombre		= $this->input->post("Nombre");
			$Imagen		= $this->input->post("Imagen");
			$Descripcion = $this->input->post("Descripcion");
			$Descripcion_Corta = $this->input->post("Descripcion_Corta");
			$this->load->model("menu_model");
			$this->menu_model->Insertar($Nombre,$Descripcion,$Descripcion_Corta,$Imagen,$Tipo);
			redirect("admin/index");
		}
		
		public function AccionesAlimentos(){
			$data['Vista'] = "Admin/AccionesAlimentos_view";
			$this->load->view('Admin/AdminLayout_view',$data);
		}
		
		public function TablaAlimentos($Tipo){
			$this->load->model("menu_model");
			if(!isset($Tipo) OR empty($Tipo)){
				$Tipo = 0;
			}
			$data['Alimentos'] = $this->menu_model->MostrarTipo($Tipo);
			$this->load->view("Admin/TablaAlimentos_view",$data);
		}
		
		public function ActualizarAlimento($Id_Alimento){
			$this->load->model("menu_model");
			$data['Alimento'] = $this->menu_model->Mostrar($Id_Alimento);
			$data['Vista'] = "Admin/ActualizarAlimento_view";
			$this->load->view('Admin/AdminLayout_view',$data);
		}
		
		public function ActualizarAlimento_POST(){
			$Id_Alimento	= $this->input->post("Id_Alimento");
			$Idioma			= $this->input->post("Idioma");
			$Tipo			= $this->input->post("Tipo");
			$Nombre			= $this->input->post("Nombre");
			$Imagen			= $this->input->post("Imagen");
			$Descripcion	= $this->input->post("Descripcion");
			$Descripcion_Corta = $this->input->post("Descripcion_Corta");
			$this->load->model("menu_model");
			$this->menu_model->Actualizar($Id_Alimento,$Nombre,$Descripcion,$Descripcion_Corta,$Imagen,$Tipo,$Idioma);
			redirect("admin/AccionesAlimentos");
		}
		
		public function EliminarAlimento($Id_Alimento){
			$this->load->model("menu_model");
			$this->menu_model->Eliminar($Id_Alimento);
		}
		
		/* Alimentos fin */
		
		/* Tipos de Vino */
		
		public function AgregarTipoVino(){
			$data['Vista'] = "Admin/AgregarTipoVino_view";
			$this->load->view('Admin/AdminLayout_view',$data);	
		}
		
		public function AgregarTipoVino_POST(){
			$Descripcion	= $this->input->post('Descripcion');
			$Idioma 		= $this->input->post('Idioma');
			$this->load->model('tipos_de_vino_model');
			$this->tipos_de_vino_model->Insertar($Descripcion,$Idioma);
			redirect('admin/index');
		}
		
		public function AccionesTipoVino(){
			$data['Vista']= "Admin/AccionesTipoVino_view";
			$this->load->view("Admin/AdminLayout_view",$data);
		}
		
		public function TablaTipoVino($Idioma){
			$this->load->model('tipos_de_vino_model');
			$data['TipoVino']= $this->tipos_de_vino_model->MostrarPorIdioma($Idioma);
			$this->load->view('Admin/TablaTipoVino_view',$data);
		}
		
		public function EliminarTipoVino($Id_Tipo){
			$this->load->model("tipos_de_vino_model");
			$this->tipos_de_vino_model->Eliminar($Id_Tipo);
		}

		public function ActualizarTipoVino($Id_Tipo){
			$this->load->model("tipos_de_vino_model");
			$data['TipoVino'] = $this->tipos_de_vino_model->Mostrar($Id_Tipo);
			$data['Vista'] = "Admin/ActualizarTipoVino_view";
			$this->load->view('Admin/AdminLayout_view',$data);
		}
		
		public function ActualizarTipoVino_POST(){
			$Id_TipoVino= $this->input->post("Id_TipoVino");
			$Idioma 	= $this->input->post("Idioma");
			$Descripcion= $this->input->post("Descripcion");
			$this->load->model("tipos_de_vino_model");
			$this->tipos_de_vino_model->Actualizar($Id_TipoVino,$Descripcion,$Idioma);
			redirect("admin/AccionesTipoVino");
		}
		/* Tipos de Vino fin */
		
		/* Nacionalidades */
		
		public function AgregarNacionalidad(){
			$data['Vista'] = "Admin/AgregarNacionalidad_view";
			$this->load->view('Admin/AdminLayout_view',$data);	
		}
		
		public function AgregarNacionalidad_POST(){
			$Nombre = $this->input->post('Nombre');
			$Imagen = $this->input->post('Imagen');
			$this->load->model('nacionalidades_model');
			$this->nacionalidades_model->Insertar($Nombre,$Imagen);
			redirect('admin/index');
		}
		
		public function AccionesNacionalidades(){
			$data['Vista']= "Admin/AccionesNacionalidades_view";
			$this->load->view("Admin/AdminLayout_view",$data);
		}
		
		public function TablaNacionalidades(){
			$this->load->model('nacionalidades_model');
			$data['Nacionalidades']= $this->nacionalidades_model->MostrarTodo();
			$this->load->view('Admin/TablaNacionalidades_view',$data);
		}
		
		public function EliminarNacionalidad($Id_Nacionalidad){
			$this->load->model("nacionalidades_model");
			$this->nacionalidades_model->Eliminar($Id_Nacionalidad);
		}
		
		public function ActualizarNacionalidad($Id_Nacionalidad){
			$this->load->model("nacionalidades_model");
			$data['Nacionalidad'] = $this->nacionalidades_model->Mostrar($Id_Nacionalidad);
			$data['Vista'] = "Admin/ActualizarNacionalidad_view";
			$this->load->view('Admin/AdminLayout_view',$data);
		}
		
		public function ActualizarNacionalidad_POST(){
			$Id_Nacionalidad= $this->input->post("Id_Nacionalidad");
			$Nombre			= $this->input->post("Nombre");
			$Imagen			= $this->input->post("Imagen");
			$this->load->model("nacionalidades_model");
			$this->nacionalidades_model->Actualizar($Id_Nacionalidad,$Nombre,$Imagen);
			redirect("admin/AccionesNacionalidades");
		}
		/* Nacionalidades fin */
		
		/* Vinos */
		public function AgregarVino(){
			$data['Vista'] = "Admin/AgregarVino_view";
			$this->load->model("nacionalidades_model");
			$this->load->model("tipos_de_vino_model");
			$data['Nacionalidades'] = $this->nacionalidades_model->MostrarTodo();
			$data['TipoVino'] = $this->tipos_de_vino_model->MostrarPorIdioma("TODOS");
			$this->load->view('Admin/AdminLayout_view',$data);	
		}
		
		public function AgregarVino_POST(){
			$Idioma			= $this->input->post('Idioma');
			$Id_Nacionalidad= $this->input->post('Id_Nacionalidad');
			$Id_Tipo_De_Vino= $this->input->post('Id_Tipo_De_Vino');
			$Precio			= $this->input->post('Precio');
			$Descripcion	= $this->input->post('Descripcion');
			$this->load->model('vinos_model');
			$this->vinos_model->Insertar($Precio,$Descripcion,$Idioma,$Id_Nacionalidad,$Id_Tipo_De_Vino);
			redirect('admin/index');
		}
		
		public function AccionesVinos(){
			$data['Vista']= "Admin/AccionesVinos_view";
			$this->load->model("nacionalidades_model");
			$this->load->model("tipos_de_vino_model");
			$data['Nacionalidades'] = $this->nacionalidades_model->MostrarTodo();
			$data['TipoVino'] = $this->tipos_de_vino_model->MostrarPorIdioma("TODOS");
			$this->load->view("Admin/AdminLayout_view",$data);
		}

		public function TablaVinos($Idioma,$Id_Nacionalidad,$Id_Tipo_De_Vino){
			$this->load->model('vinos_model');
			$data['Vinos']= $this->vinos_model->MostrarPorIdiomaNacionTipo($Idioma,$Id_Nacionalidad,$Id_Tipo_De_Vino);
			$this->load->view('Admin/TablaVinos_view',$data);
		}
		
		public function EliminarVino($Id_Vino){
			$this->load->model("vinos_model");
			$this->vinos_model->Eliminar($Id_Vino);
		}
		
		public function ActualizarVino($Id_Vino){
			$this->load->model("vinos_model");
			$this->load->model("nacionalidades_model");
			$this->load->model("tipos_de_vino_model");
			$data['Nacionalidades'] = $this->nacionalidades_model->MostrarTodo();
			$data['TipoVino'] 		= $this->tipos_de_vino_model->MostrarPorIdioma("TODOS");
			$data['Vino'] 			= $this->vinos_model->Mostrar($Id_Vino);
			$data['Vista'] = "Admin/ActualizarVino_view";
			$this->load->view('Admin/AdminLayout_view',$data);
		}
		
		public function ActualizarVino_POST(){
			$Idioma			= $this->input->post('Idioma');
			$Id_Nacionalidad= $this->input->post('Id_Nacionalidad');
			$Id_Tipo_De_Vino= $this->input->post('Id_Tipo_De_Vino');
			$Precio			= $this->input->post('Precio');
			$Descripcion	= $this->input->post('Descripcion');
			$Id_Vino		= $this->input->post("Id_Vino");
			$this->load->model("vinos_model");
			$this->vinos_model->Actualizar($Id_Vino,$Precio,$Descripcion,$Idioma,$Id_Nacionalidad,$Id_Tipo_De_Vino);
			redirect("admin/AccionesVinos");
		}
		/* Vinos fin */
		
		/* Reservaciones */
		public function AccionesReservaciones(){
			$data['Vista']= "Admin/AccionesReservaciones_view";
			$this->load->view("Admin/AdminLayout_view",$data);
		}
		public function TablaReservaciones($Estatus){
			$this->load->model('reservaciones_model');
			$data['Reservaciones']= $this->reservaciones_model->MostrarPorEstatus($Estatus);
			$this->load->view('Admin/TablaReservaciones_view',$data);
		}
		public function ActualizarReservacion($Id_Reservacion){
			$this->load->model("reservaciones_model");
			$data['Reservacion']= $this->reservaciones_model->Mostrar($Id_Reservacion);
			$data['Vista'] 		= "Admin/ActualizarReservacion_view";
			$this->load->view('Admin/AdminLayout_view',$data);
		}
		public function ActualizarReservacion_POST(){
			$Fecha_Reg		= $this->input->post("Fecha_Reg");
			$Estatus		= $this->input->post("Estatus");
			$Id_Reservacion	= $this->input->post("Id_Reservacion");
			$this->load->model("reservaciones_model");
			$this->reservaciones_model->CambiarEstatus($Id_Reservacion,$Fecha_Reg,$Estatus);
			redirect("admin/AccionesReservaciones");
		}
		/* Reservaciones fin */
		
		/* Galerias */
		public function AgregarGaleria(){
			$data['Vista'] 	= "Admin/AgregarGaleria_view";
			$this->load->view('Admin/AdminLayout_view',$data);
		}
		
		public function AgregarGaleria_POST(){
			$Titulo		= $this->input->post("Titulo");
			$URL_Tapa	= $this->input->post("URL_Tapa");
			$this->load->model("galerias_model");
			$Id_Galeria = $this->galerias_model->Insertar($Titulo,$URL_Tapa);
			$this->output->set_header('Content-Type: application/json; charset=utf-8');
  			echo json_encode(array('Id_Galeria'=>$Id_Galeria));
		}
		
		public function ActualizarGaleria($Id_Galeria){
			$this->load->model("galerias_model");
			$data['Galeria'] 	= $this->galerias_model->Obtener($Id_Galeria);
			$data['Vista'] 		= "Admin/ActualizarGaleria_view";
			$this->load->view('Admin/AdminLayout_view',$data);
		}

		public function ActualizarGaleria_POST(){
			$Id_Galeria	= $this->input->post("Id_Galeria");
			$Titulo		= $this->input->post("Titulo");
			$URL_Tapa	= $this->input->post("URL_Tapa");
			$this->load->model("galerias_model");
			$Id_Galeria = $this->galerias_model->Actualizar($Id_Galeria,$Titulo,$URL_Tapa);
			$this->output->set_header('Content-Type: application/json; charset=utf-8');
  			echo json_encode(array('Id_Galeria'=>$Id_Galeria));
		}
		
		public function TablaGalerias(){
			$this->load->model('galerias_model');
			$data['Galerias']= $this->galerias_model->ObtenerTodo();
			$this->load->view('Admin/TablaGalerias_view',$data);
		}
		
		public function AccionesGalerias(){
			$data['Vista']= "Admin/AccionesGalerias_view";
			$this->load->view("Admin/AdminLayout_view",$data);
		}
		
		public function EliminarGaleria($Id_Galeria){
			$this->load->model("galerias_model");
			$this->galerias_model->Eliminar($Id_Galeria);
			$this->load->model("imagenes_galeria_model");
			$this->imagenes_galeria_model->EliminarPorGaleria($Id_Galeria);
			$this->output->set_header('Content-Type: application/json; charset=utf-8');
  			echo json_encode(array('Id_Galeria'=>$Id_Galeria));
		}
		/* Galerias fin */
		
		/* Agregar Imagenes_Galeria */
		public function AgregarImagenGaleria_POST(){
			$URL_Imagen		= $this->input->post("URL_Imagen");
			$URL_ImagenMin	= $this->input->post("URL_ImagenMin");
			$Id_Galeria		= $this->input->post("Id_Galeria");
			$this->load->model("imagenes_galeria_model");
			$Id_Imagen = $this->imagenes_galeria_model->Insertar($URL_Imagen,$URL_ImagenMin,$Id_Galeria);
			
			$this->output->set_header('Content-Type: application/json; charset=utf-8');
  			echo json_encode(array('Id_Imagen'=>$Id_Imagen));
			
			/*foreach($URL_Imagen as $imagen){
				$this->imagenes_galeria_model->Insertar($imagen->URL_Imagen,$imagen->URL_ImagenMin,$Id_Galeria);
			}*/
		}
		
		public function ObtenerListaImagenes(){
			$Id_Galeria	= $this->input->post("Id_Galeria");
			$this->load->model("imagenes_galeria_model");
			$Result = $this->imagenes_galeria_model->ObtenerTodo($Id_Galeria);
			$this->output->set_header('Content-Type: application/json; charset=utf-8');
  			echo json_encode($Result);
		}
		
		public function EliminarImagen($Id_Galeria){
			$this->load->model("imagenes_galeria_model");
			$this->imagenes_galeria_model->Eliminar($Id_Galeria);
		}
		/* Agregar Imagenes_Galeria fin */
		
		public function ActualizarBanners(){
			$data['Vista']= "Admin/ActualizarBanners_view";
			$this->load->view("Admin/AdminLayout_view",$data);
		}
		
		public function CerrarSesion(){
			$this->session->sess_destroy();
			redirect("login/index");
		}
	}
?>