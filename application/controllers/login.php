<?php
    class Login extends CI_Controller 
    {
    	public function index(){
    		$this->load->view("Login/index_view");
    	} 
		
		public function IniciarSesion(){
			$Username = $this->input->post('Username');	
			$Password = $this->input->post('Password');	
			$this->load->model("usuario_model");
			$resultado = $this->usuario_model->ObtenerUsuario($Username, $Password);
			if($resultado=="false"){
				$datos['resultado']=$resultado;
				$this->load->view("Login/index_view",$datos);
			}
			else{
				redirect("admin/index");
			}
		}
		
		public function Registrar(){
			$Nombre = $this->input->post('Nombre');	
			$Apellido_paterno = $this->input->post('Apellido_paterno');	
			$Apellido_materno = $this->input->post('Apellido_materno');	
			$Username = $this->input->post('Username');	
			$Password = $this->input->post('Password');	
			$this->load->model("usuario_model");
			$result = $this->usuario_model->Registro($Nombre, $Apellido_paterno, $Apellido_materno, $Username, $Password);
			if($result=="false"){
				$this->usuario_model->ObtenerUsuario($Username, $Password);
				redirect("admin/index");
			}
			else{
				redirect("login/index");
			}
		}
	}
?>