<?php
    class Home extends CI_Controller 
    {
    	public function __construct()
       {
            parent::__construct();
       }
	   
	   public function index()
		{
			$Datos['Estilos'] = array('assets/css/galeria.css');
			$Datos['Vista'] = "Home/index_view";
			$this->load->view('Home/HomeLayout_view',$Datos);
		}
		
		public function Menu($Tipo)
		{
			$this->load->model('menu_model');
			$Datos['Alimentos']	= $this->menu_model->MostrarTipo($Tipo);
			$Datos['Estilos'] = array('assets/css/menu.css');
			$Datos['Vista'] = "Home/MostrarMenu_view";
			$this->load->view('Home/HomeLayout_view',$Datos);
		}
		
		public function Vinos(){
			$this->load->model('vinos_model');
			$this->load->model('tipos_de_vino_model');
			$this->load->model('nacionalidades_model');
			$Datos['Nacionalidades'] = $this->nacionalidades_model->MostrarTodo();
			$Datos['Categorias'] = $this->tipos_de_vino_model->MostrarPorIdioma('ES');
			$Datos['Vinos'] = $this->vinos_model->MostrarOrdernado('ES');
			$Datos['maxNaciones'] = $this->vinos_model->ContarNaciones('ES');
			$Datos['Vista'] = "Home/vinos_view";
			$Datos['Estilos'] = array('assets/css/vinos.css');
			$this->load->view('Home/HomeLayout_view',$Datos);
		}
		
		public function Contacto(){
			$Datos['Vista'] = "Home/contacto_view";
			$Datos['Estilos'] = array('assets/css/contacto.css');
			$this->load->view('Home/HomeLayout_view',$Datos);
		}
		
		public function Galerias(){
			$Datos['Vista'] 	= "Home/galerias_view";
			$Datos['Estilos'] 	= array('assets/css/galerias.css');
			$this->load->model('galerias_model');
			$Datos['Galerias']	= $this->galerias_model->ObtenerTodo();
			$this->load->view('Home/HomeLayout_view',$Datos);
		}
		
		public function Galeria($Id_Galeria){
			$Datos['Vista'] 	= "Home/galeria_view";
			$Datos['Estilos'] 	= array('assets/css/galerias.css','assets/css/colorbox.css');
			$this->load->model('imagenes_galeria_model');
			$Datos['Imagenes']	= $this->imagenes_galeria_model->ObtenerTodo($Id_Galeria);
			$this->load->view('Home/HomeLayout_view',$Datos);
		}
		
		public function Reservacion_POST(){
			$Nombre			= $this->input->post('Nombre');
			$Apellidos		= $this->input->post('Apellidos');
			$Telefono		= $this->input->post('Telefono');
			$Email			= $this->input->post('Email');
			$Fecha_Res		= $this->input->post('Fecha_Res');
			$Hora_Res		= $this->input->post('Hora_Res');
			$Num_Personas	= $this->input->post('Num_Personas');
			$Comentarios	= $this->input->post('Comentarios');
			$this->load->model("reservaciones_model");
			$this->reservaciones_model->Insertar($Nombre,$Apellidos,$Telefono,$Email,$Fecha_Res,$Hora_Res,$Num_Personas,$Comentarios);
			if(isset($Email))
			{
				$emailr = "noel_ska15@hotmail.com";
   
			    // asunto del email
			    $subject = $Nombre." - Reservación";
			   
			    // Cuerpo del mensaje
			    $mensaje = "---------------------------------- \n";
			    $mensaje.= "            Contacto               \n";
			    $mensaje.= "---------------------------------- \n";
			    $mensaje.= "NOMBRE:   ".$Nombre.' '.$Apellidos."\n";
			    $mensaje.= "EMAIL:    ".$Email."\n";
			    $mensaje.= "FECHA DEL MENSAJE:    ".date("d/m/Y")."\n";
			    $mensaje.= "HORA DEL MENSAJE:     ".date("h:i:s a")."\n";
			    $mensaje.= "---------------------------------- \n\n";
			    $mensaje.= $Comentarios."\n\n";
			    $mensaje.= "---------------------------------- \n";
			    $mensaje.= "Enviado desde wwww.mundoolaverde.org \n";
			   
			    // Enviamos el mensaje
			    if (mail($emailr, $subject, $Comentarios)) {
			        $data['aviso'] = "Su mensaje fue enviado.";
			    } else {
			        $data['aviso'] = "Error de envío.";
			    }
			}
			redirect("home/Contacto");
		}
		
	}
?>
