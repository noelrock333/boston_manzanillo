<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
<script src="<?php echo base_url("assets/js/jquery.tablesorter.js")?>"></script>
<script src="<?php echo base_url("assets/js/jquery.tablesorter.pager.js")?>"></script>
<script src="<?php echo base_url("assets/js/jquery.tablesorter.widgets.js")?>"></script>
<script type="text/javascript">
	$(document).ready(function(){
		$("#TablaVinos").load('<?php echo site_url("admin/TablaVinos"); ?>/TODOS/0/0',function(){
				tableFilter('#TBLVinos','#pager');
		});
		$("#Idioma, #Id_Nacionalidad, #Id_Tipo_De_Vino").change(function(){
			$("#TablaVinos").load('<?php echo site_url("admin/TablaVinos"); ?>/'+$("#Idioma").val()+'/'+$("#Id_Nacionalidad").val()+'/'+$("#Id_Tipo_De_Vino").val(),function(){
				tableFilter('#TBLVinos','#pager');
			});
		});
		
	});
	
	function tableFilter(tabla,paginador){
		$(tabla).tablesorter({
			widgets: ["zebra", "filter"],
			widgetOptions : {
			      filter_childRows : false,
			      filter_columnFilters : true,
			      filter_cssFilter : 'tablesorter-filter',
			      filter_filteredRow   : 'filtered',
			      filter_formatter : null,
			      filter_functions : null,
			      filter_hideFilters : false, // true, (see note in the options section above)
			      filter_ignoreCase : true,
			      filter_liveSearch : true,
			      filter_reset : 'button.reset',
			      filter_searchDelay : 300,
			      filter_serversideFiltering: false,
			      filter_startsWith : false,
			      filter_useParsedData : false
				}
			}).tablesorterPager({container: $(paginador)});
			
		$(".eliminar").click(function(event){
			event.preventDefault();
			if( confirm("¿Desea eliminar?") ){
				$.ajax({
					type: "GET",
					url: "<?php echo site_url("admin/EliminarVino") ?>/"+$(this).attr('href'),
					}).done(function() {
				});
				$(this).parent().parent().empty();
			}
		});
	}
</script>
<table class="formulario">
	<tr>
		<td>
			<span>Idioma</span>
			<select id="Idioma" >
				<option value="TODOS">TODOS</option>
				<option value="ES">ESPAÑOL</option>
				<option value="EN">INGLÉS</option>
			</select>
		</td>
		<td>
			<span>Nacionalidad</span>
			<select id='Id_Nacionalidad'>
				<option value="0"></option>
				<?php 
				foreach($Nacionalidades as $row){
					echo "<option value='".$row->Id_Nacionalidad."'>".$row->Nombre."</option>";
				}
				?>
			</select>
		</td>
		<td>
			<span>Categoria</span>
			<select id='Id_Tipo_De_Vino'>
				<option value="0"></option>
				<?php 
				foreach($TipoVino as $row){
					echo "<option value='".$row->Id_Tipo_De_Vino."'>".$row->Descripcion."</option>";
				}
				?>
			</select>
		</td>
	</tr>
	<tr>
		<td colspan="3">
		</td>
	</tr>
</table>
<div id="TablaVinos"></div>