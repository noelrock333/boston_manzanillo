<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
<link rel="stylesheet" href="<?php echo base_url('assets/css/font-awesome.min.css')?>" />
<script src="<?php echo base_url("assets/js/jquery.tablesorter.js")?>"></script>
<script src="<?php echo base_url("assets/js/jquery.tablesorter.pager.js")?>"></script>
<script src="<?php echo base_url("assets/js/jquery.tablesorter.widgets.js")?>"></script>
<script type="text/javascript">
	$(document).ready(function(){
		$("#TablaReservaciones").load('<?php echo site_url("admin/TablaReservaciones"); ?>/1',function(){
			tableFilter('#TBLReservaciones','#pager');
		});
		
		$("#Estatus").on("change",function(){
			$("#TablaReservaciones").load('<?php echo site_url("admin/TablaReservaciones"); ?>/'+$(this).val() ,function(){
				tableFilter('#TBLReservaciones','#pager');
			});
		});
	});
	
	function tableFilter(tabla,paginador){
		$(tabla).tablesorter({
			widgets: ["zebra", "filter"],
			widgetOptions : {
			      filter_childRows : false,
			      filter_columnFilters : true,
			      filter_cssFilter : 'tablesorter-filter',
			      filter_filteredRow   : 'filtered',
			      filter_formatter : null,
			      filter_functions : null,
			      filter_hideFilters : false, // true, (see note in the options section above)
			      filter_ignoreCase : true,
			      filter_liveSearch : true,
			      filter_reset : 'button.reset',
			      filter_searchDelay : 300,
			      filter_serversideFiltering: false,
			      filter_startsWith : false,
			      filter_useParsedData : false
				}
			}).tablesorterPager({container: $(paginador)});
	}
</script>
<div class="formulario" style="width:1020px;">
	Estatus: 
	<select name="Estatus" id="Estatus">
		<option value="1">PENDIENTES</option>
		<option value="2">RESERVACIONES REALIZADAS</option>
		<option value="3">POSPUESTAS</option>
		<option value="4">CANCELADAS</option>
	</select>
</div>
<div id="TablaReservaciones"></div>