<script>
	$(document).ready(function(){
		$(".eliminar").click(function(event){
			event.preventDefault();
			if( confirm("¿Desea eliminar?") ){
				$.ajax({
					type: "GET",
					url: "<?php echo site_url("admin/EliminarTipoVino") ?>/"+$(this).attr('href'),
					}).done(function() {
				});
				$(this).parent().parent().empty();
			}
		});
	});
</script>
<table class="table table-striped table-borded">
	<thead>
		<tr>
			<th>Descripción</th>
			<th>Idioma</th>
			<th colspan="2">Acciones</th>
		</tr>
	</thead>
<?php
if(isset($TipoVino)){
	foreach ($TipoVino as $row){
		echo '<tr>';
		echo '<td>'.$row->Descripcion.'</td>';
		echo '<td>'.$row->Idioma.'</td>';
		echo '<td><a href="'.$row->Id_Tipo_De_Vino.'" class="eliminar">Eliminar</a></td>';
		echo '<td><a href="'.site_url("admin/ActualizarTipoVino")."/".$row->Id_Tipo_De_Vino.'">Modificar</a></td>';
		echo '</tr>';
	}
}
?>
</table>