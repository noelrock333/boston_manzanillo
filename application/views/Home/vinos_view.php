<script src="<?php echo base_url("assets/js/jquery.formatCurrency-1.4.0.min.js")?>" type="text/javascript"></script>
<script> 
	$(document).ready(function(){
		$('.moneda').formatCurrency();
		
		adaptaTabla();
		$(window).on('resize',function(){
			adaptaTabla();
		});
		
	});
	
	function adaptaTabla(){
		if($(window).width()<800){
			$(".VinoHead").on('click',function(){
				$(this).parent().find('.TablaVinos').toggle('slow');
			});
		}
		else{
			$(".VinoHead").off();
			$('.TablaVinos').show();
		}
	}
</script>
<?php 
	$numNaciones = count($maxNaciones);
	$Id_NacionalidadAux = 0;
	$Id_TipoDe_VinoAux = 0;
	$lista = "";
	$contTipos = 0;
	$contNaciones=0;
	$inicioTabla = false;
	foreach($Vinos as $row){
		if($Id_NacionalidadAux != $row->Id_Nacionalidad){
			$Id_NacionalidadAux = $row->Id_Nacionalidad;
			if($contNaciones>0 AND $contNaciones<$numNaciones){
				echo '	</table>';
				echo '</div>
				';
				$inicioTabla = false;
			}
			echo '<div class="VinoPaises">';
			echo '<div class="VinoHead">
					<div class="iconVino2"></div>
					<div class="NombreNacion">'.ObtenerNacionaldiad($Nacionalidades,$row->Id_Nacionalidad)->Nombre.'</div>
					<img src="'.ObtenerNacionaldiad($Nacionalidades,$row->Id_Nacionalidad)->Imagen.'" class="iconBandera" />
				  </div>
				  <hr class="hrVinos"></hr><br />';
			if($inicioTabla == false){
				echo '<table class="TablaVinos">';
				$inicioTabla = true;
			}
			$contTipos = 0;
			$contNaciones++;
		}
		if($Id_TipoDe_VinoAux != $row->Id_Tipo_De_Vino){
			$Id_TipoDe_VinoAux = $row->Id_Tipo_De_Vino;
			if($contTipos==0 && $inicioTabla == false){
				echo '<table class="TablaVinos">';
			}
			echo '<tr class="TipoVino">
					<td class="columnaUno">Botella</td>
					<td>'.ObtenerCategoria($row->Id_Tipo_De_Vino,$Categorias).'</td>
				  </tr>';
			$contTipos++;
		}
		
		echo '<tr>
				<td class="columnaUno moneda">'.$row->Precio.'</td>
				<td>'.$row->Descripcion.'</td>
			  </tr>
			  ';
			  
	}
	echo '	</table>
		</div>';
	
	function ObtenerNacionaldiad($Nacionalidades,$Id_Nac){
		foreach($Nacionalidades as $Nac){
			if($Nac->Id_Nacionalidad == $Id_Nac){
				return $Nac;
			}
		}
	}
	
	function ObtenerCategoria($Id_Tipo,$Categorias){
		foreach($Categorias as $Categ){
			if($Categ->Id_Tipo_De_Vino == $Id_Tipo){
				return $Categ->Descripcion;
			}
		}
	}
?>