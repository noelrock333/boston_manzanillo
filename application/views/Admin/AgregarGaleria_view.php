<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.0/jquery.min.js"></script>
<script type="text/javascript" src="<?php echo base_url('AjaxFileUploader/ajaxfileupload.js') ?>"></script>
<script type="text/javascript">
$(document).ready(function(){
	
	if($("#albumCreado").val()!=""){
		$("#imagenesGaleria").show();
		$("#ImagenSubida").html("<i class='icon-ok icon-large'></i>&nbsp;&nbsp;Con imagen").removeClass("SinImagen").addClass("ConImagen");
		rellenaTablaImagenes($("#albumCreado").val());
	}
	
	$("#CrearAlbum").on("click",function(){
		if( $.trim($("#Titulo").val())!="" && $("#URL_Tapa").val()!="" ){
		    $.ajax({
				type: "POST",
				url: "<?php echo site_url('admin/AgregarGaleria_POST')?>",
				data: { Titulo: $("#Titulo").val(), URL_Tapa: $("#URL_Tapa").val() }
			}).done(function(data) {
				//console.log( data.Id_Galeria ); //Id de la galeria creada
				if(data.Id_Galeria>0){
					crearDirectorio(data.Id_Galeria);
				}
			});
		}
		else{
			var mensaje="";
			$(".requerido1").each(function(){
				if($.trim($(this).val())=="")
					mensaje += "\t"+$(this).attr("alt")+"\n";
			});
			if(mensaje!=""){
				mensaje="Falta:\n"+mensaje;
				alert(mensaje);
			}
		}
	});
	
});

function crearDirectorio(Id_Galeria){
	$.ajax({
		url: "<?php echo base_url('AjaxFileUploader/CreaGalerias.php?Dir=Galeria')?>"+Id_Galeria,
		secureuri:false,
		dataType: 'json',
		success: function (data){
			if(typeof(data.error) != 'undefined'){
				alert("Album creado con exito");
				$("#CrearAlbum").off().attr('disabled', 'disabled');
				$("#Titulo").attr('disabled', 'disabled');
				$("#buttonUpload").attr('disabled', 'disabled');
				$("#albumCreado").val(Id_Galeria);
				$("#imagenesGaleria").show();
			}
		}
	});
}

function ajaxFileUpload()
{
	$("#loading")
	.ajaxStart(function(){
		$(this).show();
	})
	.ajaxComplete(function(){
		$(this).hide();
	});

	$.ajaxFileUpload({
			url:'<?php echo base_url('AjaxFileUploader/doajaxfileupload.php') ?>',
			secureuri:false,
			fileElementId:'fileToUpload',
			dataType: 'json',
			data:{name:'logan', id:'id'},
			success: function (data, status){
				if(typeof(data.error) != 'undefined'){
					if(data.error != ''){
						alert(data.error);
						$("#ImagenSubida").text("Sin Imagen").removeClass("ConImagen").addClass("SinImagen");
						$("#URL_Tapa").val("");
					}
					else{
						$("#ImagenSubida").html("<i class='icon-ok icon-large'></i>&nbsp;&nbsp;Con imagen").removeClass("SinImagen").addClass("ConImagen");
						$("#URL_Tapa").val('assets/images/subidas/'+data.fname);
					}
				}
			},
			error: function (data, status, e){
				alert(e);
			}
	})
	return false;
}

function ajaxImagenGaleria()
{
	$("#loading")
	.ajaxStart(function(){
		$(this).show();
	})
	.ajaxComplete(function(){
		$(this).hide();
	});

	$.ajaxFileUpload({
			url:'<?php echo base_url('AjaxFileUploader/SubeImagenGaleria.php') ?>',
			secureuri:false,
			fileElementId:'fileToUploadImg',
			dataType: 'json',
			data:{name:'noel', id:'id',Dir:$("#albumCreado").val(),fileElement:'fileToUploadImg'},
			success: function (data, status){
				if(typeof(data.error) != 'undefined'){
					if(data.error != ''){
						alert(data.error);
						$("#URL_Imagen").val("");
					}
					else{
						agregaImagenes(data.rutag,data.rutach,$("#albumCreado").val());
						/*$("#URL_Imagen").val('<?php echo base_url('/assets/images/subidas')?>'+'/'+data.fname);*/
					}
				}
			},
			error: function (data, status, e){
				alert(e);
			}
	})
	return false;
}

function agregaImagenes(URL_Imagen,URL_ImagenMin,Id_Galeria){
	$.ajax({
		url: "<?php echo site_url('admin/AgregarImagenGaleria_POST')?>",
		type:"POST",
		dataType: 'json',
		data:{ URL_Imagen:URL_Imagen,URL_ImagenMin:URL_ImagenMin,Id_Galeria:Id_Galeria},
		success: function (data){
			if(data.Id_Imagen>0)
				var estructura ='';
				estructura =  '<tr class="rowImagenes">';
				//estructura +=	'<td>'+item.Id_Imagen+'</td>';
				estructura +=	'<td>'+URL_Imagen+'</td>';
				estructura +=	'<td>'+URL_ImagenMin+'</td>';
				estructura +=	'<td><a href="'+data.Id_Imagen+'" class="eliminar">Eliminar</a></td>';
				estructura += '</tr>';
				$("#imagenesGaleriaResult tbody").append(estructura);
				activaEliminar();
		}
	});
}

function rellenaTablaImagenes(Id_Galeria){
	$.ajax({
		url: "<?php echo site_url('admin/ObtenerListaImagenes')?>",
		type:"POST",
		dataType: 'json',
		data:{ Id_Galeria:Id_Galeria },
		success: function (data){
			$.each(data, function(i, item) {
				//$("#imagenesGaleriaResult").append(data[i].PageName);
				var estructura ='';
				estructura =  '<tr class="rowImagenes">';
				//estructura +=	'<td>'+item.Id_Imagen+'</td>';
				estructura +=	'<td>'+item.URL_Imagen+'</td>';
				estructura +=	'<td>'+item.URL_ImagenMin+'</td>';
				estructura +=	'<td><a href="'+item.Id_Imagen+'" class="eliminar">Eliminar</a></td>';
				estructura += '</tr>';
				$("#imagenesGaleriaResult tbody").append(estructura);
			});
			activaEliminar();
		}
	});
}

function activaEliminar(){
	$(".eliminar").off();
	$(".eliminar").on('click',function(event){
		event.preventDefault();
		if( confirm("¿Desea eliminar?") ){
			$.ajax({
				type: "GET",
				url: "<?php echo site_url("admin/EliminarImagen") ?>/"+$(this).attr('href'),
				}).done(function() {
			});
			$(this).parent().parent().empty();
		}
	});
}
</script>
<input type="hidden" id="albumCreado" value="" />
<center><h3>Creación de galeria</h3></center>
<table class="formulario" style="width: 900px;">
	<tr>
		<td>
			<label>Titulo</label>
			<input type="text" name="Titulo" id="Titulo" class="requerido1" alt="Titulo"/>
		</td>
	</tr>
	<tr>
		<td>
			<label>Tapa de album</label>
			<input id="fileToUpload" type="file" size="45" name="fileToUpload" class="input">
			<button class="button" id="buttonUpload" onclick="return ajaxFileUpload();"><i class="icon-cloud-upload icon-large"></i>&nbsp;&nbsp;&nbsp;Subir</button>
			<input type="hidden" name="URL_Tapa" id="URL_Tapa" class="requerido1" alt="Tapa de album"/>
			<span id="ImagenSubida" class="SinImagen">Sin imagen</span>
		</td>
	</tr>
	<tr>
		<td><button id="CrearAlbum" >Crear Album</button></td>
	</tr>
</table>
<div id="imagenesGaleria" class="formulario" style="width: 900px; display:none;">
	<label>Nueva imagen</label>
	<input id="fileToUploadImg" type="file" size="45" name="fileToUploadImg" class="input">
	<button class="button" id="buttonUploadImg" onclick="return ajaxImagenGaleria();"><i class="icon-plus icon-large"></i>&nbsp;&nbsp;&nbsp;Agregar</button>
	<input type="hidden" name="URL_Imagen" id="URL_Imagen" alt="Imagen"/>
	<br />
	<br />
	<table id="imagenesGaleriaResult" style="width:100%;" class="table table-borded table-striped ">
		<thead>
			<tr>
				<!-- <th align="left">Id imagen</th> -->
				<th align="left">Grande</th>
				<th align="left">Chica</th>
				<th>Acciones</th>
			</tr>
		</thead>
		<tbody>
		</tbody>
	</table>
</div>