<?php
    class Reservaciones_model extends CI_Model {

	
	function __construct() {
		 $this->load->database();
		 parent::__construct();
	}

	function Insertar($Nombre,$Apellidos,$Telefono,$Email,$Fecha_Res,$Hora_Res,$Num_Personas,$Comentarios){
		$this->load->helper('date');
		$now = time();
		$Datos = array(
			'Nombre'=>$Nombre,
			'Apellidos'=> $Apellidos,
			'Telefono'=> $Telefono,
			'Email'=> $Email,
			'Fecha_Res'=> $Fecha_Res,
			'Hora_Res'=> $Hora_Res,
			'Num_Personas'=> $Num_Personas,
			'Comentarios'=> $Comentarios,
			'Ultima_Modificacion' => date('Y-m-d H:i:s', now())
		);
		$this->db->insert("reservaciones",$Datos);
		$this->db->close();		
	}	

	function Actualizar($Id_Reservacion,$Nombre,$Apellidos,$Telefono,$Email,$Fecha_Res,$Hora_Res,$Num_Personas,$Comentarios){
		$Datos = array(
			'Nombre'=> $Nombre,
			'Apellidos'=> $Apellidos,
			'Telefono' => $Telefono,
			'Email'=> $Email,
			'Fecha_Res'=> $Fecha_Res,
			'Hora_res'=> $Hora_Res,
			'Num_Personas'=> $Num_Personas,
			'Comentarios'=> $Comentarios
		);
		$this->db->where('Id_Reservacion',$Id_Reservacion);
		$this->db->update('reservaciones',$Datos);
		$this->db->close();
	}
	
	function CambiarEstatus($Id_Reservacion,$Fecha_Res,$Estatus){
		if($Id_Reservacion==3){
			$Datos = array(
				'Fecha_Res'	=> $Fecha_Res,
				'Estatus'	=> $Estatus
			);
		}
		else{
			$Datos = array(
				'Estatus'	=> $Estatus
			);
		}
		$this->db->where('Id_Reservacion',$Id_Reservacion);
		$this->db->update('reservaciones',$Datos);
		$this->db->close();
	}
	
	function Mostrar($Id_Reservacion){
		$this->db->select('*'); //regresa todos los campos
		$this->db->from('reservaciones');
		$this->db->where('Id_Reservacion',$Id_Reservacion);
		$resultado = $this->db->get();
		$this->db->close();
		return $resultado->result(); // despues del return solo es codigo inaccesible
	}
	
	function Eliminar($Id_Reservacion){		
		$this->db->where('Id_Reservacion',$Id_Reservacion);
		$this->db->delete('reservaciones');
		$this->db->close();
	}
	
	function MostrarFechaRes($Fecha_Res){
		$this->db->select('*');
		$this->db->from('reservaciones');
		$this->db->where('Fecha_Res',$Fecha_Res);
		$resultado = $this->db->get();
		$this->db->close();
		return $resultado->result();
	}
	
	function MostrarPorEstatus($Estatus){
		$this->db->select('*');
		$this->db->from('reservaciones');
		$this->db->where('Estatus',$Estatus);
		$this->db->order_by("Ultima_Modificacion","dec");
		$resultado = $this->db->get();
		$this->db->close();
		return $resultado->result();
	}
	
	function MostrarHoraRes($Hora_Res){
		$this->db->select('*');
		$this->db->from('reservaciones');
		$this->db->where('Hora_Res',$Hora_Res);
		$resultado = $this->db->get();
		$this->db->close();
		return $resultado->result();
	}
}
?>