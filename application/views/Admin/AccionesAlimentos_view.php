<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.0/jquery.min.js"></script>
<link rel="stylesheet" href="<?php echo base_url('assets/css/font-awesome.min.css')?>" />
<script src="<?php echo base_url("assets/js/jquery.tablesorter.js")?>"></script>
<script src="<?php echo base_url("assets/js/jquery.tablesorter.pager.js")?>"></script>
<script src="<?php echo base_url("assets/js/jquery.tablesorter.widgets.js")?>"></script>
<script type="text/javascript">
	$(document).ready(function(){
		$("#TablaAlimentos").load('<?php echo site_url("admin/TablaAlimentos"); ?>/0',function(){
			tableFilter('#TBLAlinetos','#pager');
		});
		$("#Tipo").change(function(){
			$("#TablaAlimentos").load('<?php echo site_url("admin/TablaAlimentos"); ?>/'+$("#Tipo").val(),function(){
				tableFilter('#TBLAlinetos','#pager');
			});
		});
		
	});
	
	function tableFilter(tabla,paginador){
		$(tabla).tablesorter({
			widgets: ["zebra", "filter"],
			widgetOptions : {
			      filter_childRows : false,
			      filter_columnFilters : true,
			      filter_cssFilter : 'tablesorter-filter',
			      filter_filteredRow   : 'filtered',
			      filter_formatter : null,
			      filter_functions : null,
			      filter_hideFilters : false, // true, (see note in the options section above)
			      filter_ignoreCase : true,
			      filter_liveSearch : true,
			      filter_reset : 'button.reset',
			      filter_searchDelay : 300,
			      filter_serversideFiltering: false,
			      filter_startsWith : false,
			      filter_useParsedData : false
				}
			}).tablesorterPager({container: $(paginador)});
			
		$(".eliminar").click(function(event){
			event.preventDefault();
			if( confirm("¿Desea eliminar?") ){
				$.ajax({
					type: "GET",
					url: "<?php echo site_url("admin/EliminarAlimento") ?>/"+$(this).attr('href'),
					}).done(function() {
				});
				$(this).parent().parent().empty();
			}
		});
	}
</script>
<div class="formulario" style="width: 900px;">
	<span>Tipo</span>
	<select name="Tipo" id="Tipo">
		<option value="0"></option>
		<option value="1">DESAYUNO</option>
		<option value="2">COMIDA/CENA</option>
		<option value="3">POSTRE</option>
		<option value="4">BEBIDA</option>
	</select>
</div>
<div id="TablaAlimentos"></div>
