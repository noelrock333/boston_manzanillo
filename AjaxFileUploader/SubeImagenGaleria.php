<?php
	$error = "";
	$msg = "";
	$filename="";
	$exist="";
	$routeb ="";
	$routesm ="";
	$Directorio = isset($_POST["Dir"]) ? "Galeria".$_POST["Dir"] : NULL;
	
	$fileElementName = isset($_POST["fileElement"]) ? $_POST["fileElement"] : NULL;
	if(!empty($_FILES[$fileElementName]['error']))
	{
		switch($_FILES[$fileElementName]['error'])
		{

			case '1':
				$error = 'El archivo excede el tamaño en la directiva upload_max_filesize en php.ini';
				break;
			case '2':
				$error = 'El archivo excede el tamaño maximo especificado en el formulario';
				break;
			case '3':
				$error = 'El archivo fue parcialmente cargado';
				break;
			case '4':
				$error = 'No se cargó ningun archivo';
				break;
			case '6':
				$error = 'Falta un directorio temporal';
				break;
			case '7':
				$error = 'Falla al escribir archivo en disco';
				break;
			case '8':
				$error = 'Carga detenida por extención';
				break;
			case '999':
			default:
				$error = 'Codigo de error no disponible';
		}
	}elseif(empty($_FILES[$fileElementName]['tmp_name']) || $_FILES[$fileElementName]['tmp_name'] == 'none')
	{
		$error = 'No file was uploaded..';
	}else{
		$msg .= " File Name: " . $_FILES[$fileElementName]['name'] . ", ";
		$msg .= " File Size: " . @filesize($_FILES['fileToUpload']['tmp_name']);
		$filename = "assests/images/subidas/".$Directorio."/BigSize/".$_FILES[$fileElementName]['name'];
		//for security reason, we force to remove all uploaded file
		@unlink($_FILES[$fileElementName]);
		
		if (!file_exists($filename)){
			$imagen = $_FILES[$fileElementName]['name'];
			move_uploaded_file($_FILES[$fileElementName]['tmp_name'], "../assets/images/subidas/".$Directorio."/BigSize/".$imagen);
			$url = "../assets/images/subidas/".$Directorio."/BigSize/" . $imagen;
			chmod($url, 0777);
			//--Inicia miniaturas--
			require_once 'thumbphp/ThumbLib.inc.php'; 
			$orgpath = "../assets/images/subidas/".$Directorio."/BigSize/".$imagen; 
			$thumb = PhpThumbFactory::create($orgpath);
			$newpath = "../assets/images/subidas/".$Directorio."/SmallSize/".$imagen;
			$thumb->adaptiveResize(470,253)->save($newpath); //resize tiene 2 parametros AnchuraMaxima y AlturaMaxima   
			//--Termina redimencionado---
			$routeb	= "assets/images/subidas/".$Directorio."/BigSize/".$imagen;
			$routesm= "assets/images/subidas/".$Directorio."/SmallSize/".$imagen;
		}
		else{
			$msg = "El archivo ya existe";
		}
	}
	echo json_encode(array(
		'error'	=> $error,
		'msg'	=> $msg,
		'rutag' => $routeb,
		'rutach'=> $routesm,
		'Dir' 	=> $Directorio
	));
?>