<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.0/jquery.min.js"></script>
<script src="<?php echo base_url("assets/js/jquery.formatCurrency-1.4.0.min.js")?>" type="text/javascript"></script>
<script> 
	$(document).ready(function(){
		$("#Idioma").val($("#IdiomaOld").val());
		$("#Id_Nacionalidad").val($("#Id_NacionalidadOld").val());
		$("#Id_Tipo_De_Vino").val($("#Id_Tipo_De_VinoOld").val());
		
		$('.moneda').formatCurrency();
		$('.moneda').change(function(){
			$(this).formatCurrency();
		});
		$('form').submit(function(){
			$('.moneda').toNumber();
		});
	});	
</script>
<?php echo form_open("admin/ActualizarVino_POST") ?>
<table class='formulario'>
	<tr>
		<td>
			<span>Idioma</span>
			<select name="Idioma" id="Idioma">
				<option value="ES">ESPAÑOL</option>
				<option value="EN">INGLÉS</option>
			</select>
			<input type="hidden" id="IdiomaOld" value="<?php echo $Vino[0]->Idioma?>" />
		</td>
	</tr>
	<tr>
		<td>
			<span>Nacionalidad</span>
			<select name="Id_Nacionalidad" id="Id_Nacionalidad">
				<?php 
				foreach($Nacionalidades as $row){
					echo "<option value='".$row->Id_Nacionalidad."'>".$row->Nombre."</option>";
				}
				?>
			</select>
			<input type="hidden" id="Id_NacionalidadOld" value="<?php echo $Vino[0]->Id_Nacionalidad?>" />
		</td>
	</tr>
	<tr>
		<td>
			<span>Categoria</span>
			<select name="Id_Tipo_De_Vino" id="Id_Tipo_De_Vino">
				<option value='0'></option>
				<?php 
				foreach($TipoVino as $row){
					echo "<option value='".$row->Id_Tipo_De_Vino."'>".$row->Descripcion."</option>";
				}
				?>
			</select>
			<input type="hidden" id="Id_Tipo_De_VinoOld" value="<?php echo $Vino[0]->Id_Tipo_De_Vino?>" />
		</td>
	</tr>
	<tr>
		<td>
			<span>Precio</span>
			<input type="text" name="Precio" value="<?php echo $Vino[0]->Precio?>" class="moneda" />
		</td>
	</tr>
	<tr>
		<td>
			<label>Descripción</label>
			<input type="text" name="Descripcion" value="<?php echo $Vino[0]->Descripcion?>"/>
		</td>
	</tr>
	<tr>
		<td>
			<input type="submit" value="Guardar" /><input type="hidden" name="Id_Vino" value="<?php echo $Vino[0]->Id_Vino?>"/>
		</td>
	</tr>
</table>
</form>