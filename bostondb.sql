-- phpMyAdmin SQL Dump
-- version 3.5.1
-- http://www.phpmyadmin.net
--
-- Servidor: localhost
-- Tiempo de generación: 18-09-2013 a las 05:51:54
-- Versión del servidor: 5.5.24-log
-- Versión de PHP: 5.3.13

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de datos: `bostondb`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `galerias`
--

CREATE TABLE IF NOT EXISTS `galerias` (
  `Id_Galeria` int(11) NOT NULL AUTO_INCREMENT,
  `Titulo` varchar(200) DEFAULT NULL,
  `URL_Tapa` varchar(350) DEFAULT NULL,
  `Contenido` text DEFAULT NULL,
  `Fecha` date DEFAULT NULL,
  PRIMARY KEY (`Id_Galeria`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=73 ;

--
-- Volcado de datos para la tabla `galerias`
--

INSERT INTO `galerias` (`Id_Galeria`, `Titulo`, `URL_Tapa`, `Fecha`) VALUES
(9, 'titulo galeria', 'http://localhost/boston_manzanillo/assets/images/subidas/06-org.jpg', '2013-09-08'),
(10, 'titulo galeria', 'http://localhost/boston_manzanillo/assets/images/subidas/06-org.jpg', '2013-09-08'),
(11, 'titulo galeria', 'http://localhost/boston_manzanillo/assets/images/subidas/06-org.jpg', '2013-09-08'),
(12, 'titulo galeria', 'http://localhost/boston_manzanillo/assets/images/subidas/06-org.jpg', '2013-09-08'),
(13, 'titulo galeria', 'http://localhost/boston_manzanillo/assets/images/subidas/06-org.jpg', '2013-09-08'),
(24, 'NORMATIVIDAD', 'http://localhost/boston_manzanillo/assets/images/subidas/06-org.jpg', '2013-09-08'),
(25, 'NORMATIVIDAD', 'http://localhost/boston_manzanillo/assets/images/subidas/06-org.jpg', '2013-09-08'),
(26, 'NORMATIVIDAD', 'http://localhost/boston_manzanillo/assets/images/subidas/06-org.jpg', '2013-09-08'),
(27, 'NORMATIVIDAD', 'http://localhost/boston_manzanillo/assets/images/subidas/06-org.jpg', '2013-09-08'),
(28, 'NORMATIVIDAD', 'http://localhost/boston_manzanillo/assets/images/subidas/06-org.jpg', '2013-09-08'),
(29, 'NORMATIVIDAD', 'http://localhost/boston_manzanillo/assets/images/subidas/06-org.jpg', '2013-09-08'),
(30, 'NORMATIVIDAD', 'http://localhost/boston_manzanillo/assets/images/subidas/06-org.jpg', '2013-09-08'),
(31, 'NORMATIVIDAD', 'http://localhost/boston_manzanillo/assets/images/subidas/06-org.jpg', '2013-09-08'),
(32, 'NORMATIVIDAD', 'http://localhost/boston_manzanillo/assets/images/subidas/06-org.jpg', '2013-09-08'),
(33, 'NORMATIVIDAD', 'http://localhost/boston_manzanillo/assets/images/subidas/06-org.jpg', '2013-09-08'),
(34, 'NORMATIVIDAD', 'http://localhost/boston_manzanillo/assets/images/subidas/06-org.jpg', '2013-09-08'),
(35, 'NORMATIVIDAD', 'http://localhost/boston_manzanillo/assets/images/subidas/06-org.jpg', '2013-09-08'),
(36, 'NORMATIVIDAD', 'http://localhost/boston_manzanillo/assets/images/subidas/06-org.jpg', '2013-09-08'),
(37, 'NORMATIVIDAD', 'http://localhost/boston_manzanillo/assets/images/subidas/06-org.jpg', '2013-09-08'),
(38, 'NORMATIVIDAD', 'http://localhost/boston_manzanillo/assets/images/subidas/06-org.jpg', '2013-09-08'),
(39, 'NORMATIVIDAD sdf', 'http://localhost/boston_manzanillo/assets/images/subidas/06-org.jpg', '2013-09-08'),
(40, 'NORMATIVIDAD', 'http://localhost/boston_manzanillo/assets/images/subidas/06-org.jpg', '2013-09-08'),
(41, 'NORMATIVIDAD', 'http://localhost/boston_manzanillo/assets/images/subidas/06-org.jpg', '2013-09-08'),
(42, 'NORMATIVIDAD', 'http://localhost/boston_manzanillo/assets/images/subidas/06-org.jpg', '2013-09-08'),
(43, 'NORMATIVIDAD', 'http://localhost/boston_manzanillo/assets/images/subidas/06-org.jpg', '2013-09-08'),
(44, 'NORMATIVIDAD', 'http://localhost/boston_manzanillo/assets/images/subidas/06-org.jpg', '2013-09-08'),
(45, 'NORMATIVIDAD', 'http://localhost/boston_manzanillo/assets/images/subidas/06-org.jpg', '2013-09-08'),
(46, 'NORMATIVIDAD', 'http://localhost/boston_manzanillo/assets/images/subidas/06-org.jpg', '2013-09-08'),
(47, 'NORMATIVIDAD', 'http://localhost/boston_manzanillo/assets/images/subidas/06-org.jpg', '2013-09-08'),
(48, 'NORMATIVIDAD', 'http://localhost/boston_manzanillo/assets/images/subidas/06-org.jpg', '2013-09-08'),
(49, 'NORMATIVIDAD', 'http://localhost/boston_manzanillo/assets/images/subidas/06-org.jpg', '2013-09-08'),
(50, 'NORMATIVIDAD', 'http://localhost/boston_manzanillo/assets/images/subidas/06-org.jpg', '2013-09-08'),
(51, 'NORMATIVIDAD', 'http://localhost/boston_manzanillo/assets/images/subidas/06-org.jpg', '2013-09-08'),
(52, 'NORMATIVIDAD', 'http://localhost/boston_manzanillo/assets/images/subidas/06-org.jpg', '2013-09-08'),
(53, 'NORMATIVIDAD', 'http://localhost/boston_manzanillo/assets/images/subidas/06-org.jpg', '2013-09-08'),
(54, 'NORMATIVIDAD', 'http://localhost/boston_manzanillo/assets/images/subidas/06-org.jpg', '2013-09-08'),
(55, 'NORMATIVIDAD', 'http://localhost/boston_manzanillo/assets/images/subidas/06-org.jpg', '2013-09-08'),
(56, 'NORMATIVIDAD', 'http://localhost/boston_manzanillo/assets/images/subidas/06-org.jpg', '2013-09-08'),
(57, 'NORMATIVIDAD', 'http://localhost/boston_manzanillo/assets/images/subidas/06-org.jpg', '2013-09-08'),
(58, 'NORMATIVIDAD', 'http://localhost/boston_manzanillo/assets/images/subidas/06-org.jpg', '2013-09-08'),
(59, 'NORMATIVIDAD', 'http://localhost/boston_manzanillo/assets/images/subidas/06-org.jpg', '2013-09-08'),
(60, 'NORMATIVIDAD', 'http://localhost/boston_manzanillo/assets/images/subidas/06-org.jpg', '2013-09-08'),
(61, 'NORMATIVIDAD', 'http://localhost/boston_manzanillo/assets/images/subidas/06-org.jpg', '2013-09-08'),
(62, 'NORMATIVIDAD', 'http://localhost/boston_manzanillo/assets/images/subidas/06-org.jpg', '2013-09-08'),
(63, 'NORMATIVIDAD', 'http://localhost/boston_manzanillo/assets/images/subidas/06-org.jpg', '2013-09-08'),
(64, 'NORMATIVIDAD', 'http://localhost/boston_manzanillo/assets/images/subidas/06-org.jpg', '2013-09-08'),
(65, 'NORMATIVIDAD', 'http://localhost/boston_manzanillo/assets/images/subidas/06-org.jpg', '2013-09-08'),
(66, 'NORMATIVIDAD', 'http://localhost/boston_manzanillo/assets/images/subidas/06-org.jpg', '2013-09-08'),
(67, 'NORMATIVIDAD', 'http://localhost/boston_manzanillo/assets/images/subidas/06-org.jpg', '2013-09-08'),
(68, 'NORMATIVIDAD', 'http://localhost/boston_manzanillo/assets/images/subidas/06-org.jpg', '2013-09-08'),
(69, 'rg', 'http://localhost/boston_manzanillo/assets/images/subidas/06-org.jpg', '2013-09-09'),
(70, 'NORMATIVIDAD', 'http://localhost/boston_manzanillo/assets/images/subidas/06-org.jpg', '2013-09-09'),
(71, 'Primer prueba completa', 'assets/images/subidas/GithubGray.png', '2013-09-09'),
(72, 'NORMATIVIDAD ds', 'assets/images/subidas/07-org.jpg', '2013-09-10');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `imagenes_galeria`
--

CREATE TABLE IF NOT EXISTS `imagenes_galeria` (
  `Id_Imagen` int(11) NOT NULL AUTO_INCREMENT,
  `URL_Imagen` varchar(350) DEFAULT NULL,
  `URL_ImagenMin` varchar(350) DEFAULT NULL,
  `Id_Galeria` int(11) DEFAULT NULL,
  PRIMARY KEY (`Id_Imagen`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=17 ;

--
-- Volcado de datos para la tabla `imagenes_galeria`
--

INSERT INTO `imagenes_galeria` (`Id_Imagen`, `URL_Imagen`, `URL_ImagenMin`, `Id_Galeria`) VALUES
(12, 'assets/images/subidas/Galeria71/BigSize/AlbumR1.JPG', 'assets/images/subidas/Galeria71/SmallSize/AlbumR1.JPG', 71),
(13, 'assets/images/subidas/Galeria71/BigSize/CapturaAppBluetoothAndroid.JPG', 'assets/images/subidas/Galeria71/SmallSize/CapturaAppBluetoothAndroid.JPG', 71),
(14, 'assets/images/subidas/Galeria72/BigSize/CapturaRETS1.JPG', 'assets/images/subidas/Galeria72/SmallSize/CapturaRETS1.JPG', 72),
(15, 'assets/images/subidas/Galeria72/BigSize/P9130060.JPG', 'assets/images/subidas/Galeria72/SmallSize/P9130060.JPG', 72),
(16, 'assets/images/subidas/Galeria72/BigSize/MundoOlaVerde.png', 'assets/images/subidas/Galeria72/SmallSize/MundoOlaVerde.png', 72);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `menu`
--

CREATE TABLE IF NOT EXISTS `menu` (
  `Id_Alimento` int(11) NOT NULL AUTO_INCREMENT,
  `Nombre` varchar(45) DEFAULT NULL,
  `Descripcion` text,
  `Descripcion_Corta` text,
  `Imagen` varchar(150) DEFAULT NULL,
  `Tipo` smallint(6) DEFAULT NULL,
  `Idioma` varchar(2) DEFAULT NULL,
  PRIMARY KEY (`Id_Alimento`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 COMMENT='Tabla donde se almacena el menú' AUTO_INCREMENT=4 ;

--
-- Volcado de datos para la tabla `menu`
--

INSERT INTO `menu` (`Id_Alimento`, `Nombre`, `Descripcion`, `Descripcion_Corta`, `Imagen`, `Tipo`, `Idioma`) VALUES
(2, 'deede', 'dedede', 'dedede', 'dede', 1, NULL),
(3, 'comida de prueba', 'a ver si sale el resultado', 'a ver si sale el resultado', 'http://localhost/boston_manzanillo/assets/images/subidas/AlbumR1.JPG', 2, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `nacionalidades`
--

CREATE TABLE IF NOT EXISTS `nacionalidades` (
  `Id_Nacionalidad` int(11) NOT NULL AUTO_INCREMENT,
  `Nombre` varchar(255) DEFAULT NULL,
  `Imagen` varchar(255) DEFAULT NULL,
  `Orden` int(11) DEFAULT NULL,
  PRIMARY KEY (`Id_Nacionalidad`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=12 ;

--
-- Volcado de datos para la tabla `nacionalidades`
--

INSERT INTO `nacionalidades` (`Id_Nacionalidad`, `Nombre`, `Imagen`, `Orden`) VALUES
(1, 'COPEO', '', NULL),
(2, 'MÉXICO', 'http://bostonmanzanillo.com/assets/images/subidas/icon-mex.png', NULL),
(3, 'ESPAÑA', 'http://bostonmanzanillo.com/assets/images/subidas/icon-esp.png', NULL),
(4, 'ARGENTINA', 'http://bostonmanzanillo.com/assets/images/subidas/icon-arg.png', NULL),
(5, 'CHILE', 'http://bostonmanzanillo.com/assets/images/subidas/icon-chi.png', NULL),
(6, 'EUA', 'http://bostonmanzanillo.com/assets/images/subidas/icon-usa.png', NULL),
(7, 'ITALIA', 'http://bostonmanzanillo.com/assets/images/subidas/icon-ita.png', NULL),
(8, 'FRANCIA', 'http://bostonmanzanillo.com/assets/images/subidas/icon-fra.png', NULL),
(9, 'CHAMPAGNE', 'http://bostonmanzanillo.com/assets/images/subidas/icon-fra.png', NULL),
(10, 'VINO ESPUMOSO', 'http://bostonmanzanillo.com/assets/images/subidas/icon-mex.png', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `noticias`
--

CREATE TABLE IF NOT EXISTS `noticias` (
  `Id_Noticia` int(11) NOT NULL AUTO_INCREMENT,
  `Noticia_Completa` varchar(255) DEFAULT NULL,
  `Resumen_Noticia` varchar(255) DEFAULT NULL,
  `Idioma` varchar(2) DEFAULT NULL,
  PRIMARY KEY (`Id_Noticia`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `reservaciones`
--

CREATE TABLE IF NOT EXISTS `reservaciones` (
  `Id_Reservacion` int(11) NOT NULL AUTO_INCREMENT,
  `Nombre` varchar(60) DEFAULT NULL,
  `Apellidos` varchar(100) DEFAULT NULL,
  `Telefono` varchar(45) DEFAULT NULL,
  `Email` varchar(45) DEFAULT NULL,
  `Fecha_Res` date DEFAULT NULL,
  `Hora_Res` time DEFAULT NULL,
  `Num_Personas` int(11) DEFAULT NULL,
  `Comentarios` text,
  `Ultima_Modificacion` timestamp NULL DEFAULT NULL,
  `Estatus` smallint(6) DEFAULT '1',
  PRIMARY KEY (`Id_Reservacion`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=9 ;

--
-- Volcado de datos para la tabla `reservaciones`
--

INSERT INTO `reservaciones` (`Id_Reservacion`, `Nombre`, `Apellidos`, `Telefono`, `Email`, `Fecha_Res`, `Hora_Res`, `Num_Personas`, `Comentarios`, `Ultima_Modificacion`, `Estatus`) VALUES
(1, 'usuario uno', 'garcia zepeda', '324255645', 'garcia.zep@gmail.com', NULL, NULL, 3, 'nada de comentar', NULL, 1),
(2, 'das', 'dasd', 'das', 'd', NULL, NULL, 4, 'vsdfd', '0000-00-00 00:00:00', 1),
(3, 'rwerw', 'ert', 'ert', 'ert', NULL, NULL, 0, '', '0000-00-00 00:00:00', 1),
(4, 'er', 'wer', 'werwe', 'werwer', '2013-09-02', NULL, 0, 'wer', '2013-09-02 05:34:34', 1),
(5, '', '', '', '', NULL, NULL, 0, '', '2013-09-02 05:48:26', 1),
(6, '', '', '', '', NULL, NULL, 0, '', '2013-09-02 05:54:44', 1),
(7, '', '', '', '', '2013-09-02', '00:00:00', 0, '', '2013-09-02 05:57:07', 1),
(8, '', '', '', '', '2013-09-30', '21:22:00', 0, '', '2013-09-02 05:57:33', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipos_de_vino`
--

CREATE TABLE IF NOT EXISTS `tipos_de_vino` (
  `Id_Tipo_De_Vino` int(11) NOT NULL AUTO_INCREMENT,
  `Descripcion` varchar(100) DEFAULT NULL,
  `Idioma` varchar(2) DEFAULT NULL,
  PRIMARY KEY (`Id_Tipo_De_Vino`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=9 ;

--
-- Volcado de datos para la tabla `tipos_de_vino`
--

INSERT INTO `tipos_de_vino` (`Id_Tipo_De_Vino`, `Descripcion`, `Idioma`) VALUES
(6, 'Vino Blanco', 'ES'),
(7, 'Vino Tinto', 'ES'),
(8, 'Vino Rosado', 'ES');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuario`
--

CREATE TABLE IF NOT EXISTS `usuario` (
  `Id_Usuario` int(11) NOT NULL AUTO_INCREMENT,
  `Nombre` varchar(45) DEFAULT NULL,
  `Apellido_paterno` varchar(45) DEFAULT NULL,
  `Apellido_materno` varchar(45) DEFAULT NULL,
  `Password` varchar(45) DEFAULT NULL,
  `Username` varchar(45) NOT NULL,
  PRIMARY KEY (`Id_Usuario`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Volcado de datos para la tabla `usuario`
--

INSERT INTO `usuario` (`Id_Usuario`, `Nombre`, `Apellido_paterno`, `Apellido_materno`, `Password`, `Username`) VALUES
(1, 'Administrador', 'E', 'S', '202cb962ac59075b964b07152d234b70', 'admin');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `vinos`
--

CREATE TABLE IF NOT EXISTS `vinos` (
  `Id_Vino` int(11) NOT NULL AUTO_INCREMENT,
  `Precio` float DEFAULT NULL,
  `Descripcion` varchar(255) DEFAULT NULL,
  `Id_Nacionalidad` int(255) DEFAULT NULL,
  `Id_Tipo_De_Vino` int(255) DEFAULT NULL,
  `Idioma` varchar(2) DEFAULT NULL,
  PRIMARY KEY (`Id_Vino`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=70 ;

--
-- Volcado de datos para la tabla `vinos`
--

INSERT INTO `vinos` (`Id_Vino`, `Precio`, `Descripcion`, `Id_Nacionalidad`, `Id_Tipo_De_Vino`, `Idioma`) VALUES
(3, 300.34, 'nuevo vino', 2, 5, 'EN'),
(4, 300, 'L.A. Cetto Blanc de Blancs', 2, 6, 'ES'),
(5, 1950, 'Vino de Piedra Cabernet-Tempranillo', 2, 7, 'ES'),
(7, 1800, 'Gran Ricardo Blend', 2, 7, 'ES'),
(8, 1400, 'Duetto Cabernet-Tempranillo', 2, 7, 'ES'),
(9, 350, 'L.A. Ceto Chardonnay Reserva Privada', 2, 6, 'ES'),
(10, 1350, 'Gabriel Merlot', 2, 7, 'ES'),
(11, 1300, 'Único Cabernet Merlot', 2, 7, 'ES'),
(12, 950, 'Miguel Cabernet-Syrah', 2, 7, 'ES'),
(13, 950, 'Chateau Camou Blend', 2, 7, 'ES'),
(14, 750, ' Monte Xanic Merlot', 2, 7, 'ES'),
(15, 750, 'Monte Xanic Cabernet-Merlot', 2, 7, 'ES'),
(16, 500, 'Casa Madero Shiraz', 2, 7, 'ES'),
(17, 450, 'Casa Madero 3V', 2, 7, 'ES'),
(18, 450, 'Santo Tomás Barbera', 2, 7, 'ES'),
(19, 450, 'Santo Tomás Merlot', 2, 7, 'ES'),
(20, 450, 'L.A. Cetto Nebbiola Reserva Privada', 2, 7, 'ES'),
(22, 400, 'Pazo San Mauro Albariño', 3, 6, 'ES'),
(23, 350, 'Torres Gran Viña Sol Chardonnay', 3, 6, 'ES'),
(24, 300, 'Marques del Riscal Verdejo', 3, 6, 'ES'),
(25, 1400, 'Pesquera Reserva Tempranillo', 3, 7, 'ES'),
(26, 850, 'Pesquera Tempranillo', 3, 7, 'ES'),
(27, 1400, 'Matarromera Reserva Tempranillo', 3, 7, 'ES'),
(28, 800, 'Matarromera Tempranillo', 3, 7, 'ES'),
(29, 850, 'Muga Reserva Blend', 3, 7, 'ES'),
(30, 700, 'Marques del Riscal Tempranillo', 3, 7, 'ES'),
(31, 300, 'Barefoot Chardonnay', 6, 6, 'ES'),
(32, 350, 'Beringer White Zinfandel', 6, 8, 'ES'),
(33, 550, 'Robert Mondavi Private Selection Cabernet', 6, 7, 'ES'),
(34, 350, 'Mezzacorona Pinot Grígio', 7, 6, 'ES'),
(35, 1400, 'Brunello di Montalcino Luciani', 7, 7, 'ES'),
(36, 650, 'Chianti Ruffino Riserva Ducate', 7, 7, 'ES'),
(37, 600, 'Montepulciano d Abruzzo Ruffino', 7, 7, 'ES'),
(38, 550, 'Chablis Albert Bichot', 6, 6, 'ES'),
(39, 1900, 'Chateauneuf Du Pape Calvet', 6, 7, 'ES'),
(40, 450, 'Medoc Albert Bichot', 6, 7, 'ES'),
(41, 450, 'Saint Emilion Calvet', 6, 7, 'ES'),
(42, 300, 'Beaujolais Albert Bichot', 6, 7, 'ES'),
(43, 300, 'Portillo Sauvignon Blanc', 4, 6, 'ES'),
(44, 750, 'Rutini Cabernet – Malbec', 4, 7, 'ES'),
(45, 600, 'Terrazas de los Andes Reserva Cabernet', 4, 7, 'ES'),
(46, 450, 'Trumpeter Merlot', 4, 7, 'ES'),
(47, 400, 'Punto Final Clásico Malbec', 4, 7, 'ES'),
(48, 350, 'Portillo Merlot', 4, 7, 'ES'),
(49, 650, 'Montes Alpha Chardonnay', 5, 6, 'ES'),
(50, 350, 'Cono Sur Sauvignon Blanc', 5, 6, 'ES'),
(51, 300, 'Torre Santa Digna Reserva Chardonnay', 5, 6, 'ES'),
(52, 600, 'Montgras Reserva Quatro Blend', 5, 7, 'ES'),
(53, 450, 'Santa Helena Selección del Directorio', 5, 7, 'ES'),
(54, 450, 'Carmen Carménére', 5, 7, 'ES'),
(55, 400, 'Oveja Negra Reserva Cabernet – Syrah', 5, 7, 'ES'),
(56, 350, 'TRIO Blend', 5, 7, 'ES'),
(57, 350, 'Luis Felipe Edwards Reserva Carménére', 5, 7, 'ES'),
(58, 300, 'Viña Maipo Cabernet', 5, 7, 'ES'),
(59, 450, 'Santo Tomás Cabernet', 2, 7, 'ES'),
(61, 350, 'L.A. Cetto Petite Syrah', 2, 7, 'ES'),
(62, 350, 'Marques de Arienzo Tempranillo', 3, 7, 'ES'),
(63, 350, 'Norton Malbec', 4, 7, 'ES'),
(64, 1900, 'Veuve Clicquot Rosé', 9, 7, 'ES'),
(65, 1500, 'Moet & Chandon Ice Imperial', 9, 7, 'ES'),
(66, 1400, 'Veuve Clicquot Brut', 2, 7, 'ES'),
(67, 1300, 'Moet & Chandon Imperial', 9, 7, 'ES'),
(68, 350, 'Sala Vive Brut', 10, 7, 'ES'),
(69, 300, 'Petillant Brut', 10, 7, 'ES');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
