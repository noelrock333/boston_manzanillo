<?php
    class Nacionalidades_model extends CI_Model {

	function __construct() {
		 $this->load->database();
		 parent::__construct();
	}

	function Insertar($Nombre,$Imagen){
		$Datos = array(
			'Nombre'=>$Nombre,
			'Imagen'=>$Imagen
		);
		$this->db->insert("nacionalidades",$Datos);
		$this->db->close();		
	}	

	function Actualizar($Id_Nacionalidad,$Nombre,$Imagen){
		$Datos = array(
			'Nombre'=>$Nombre,
			'Imagen'=>$Imagen
		);
		$this->db->where('Id_Nacionalidad',$Id_Nacionalidad);
		$this->db->update('nacionalidades',$Datos);
		$this->db->close();
	}
	
	function Mostrar($Id_Nacionalidad){
		$this->db->select('*'); //regresa todos los campos
		$this->db->from('nacionalidades');
		$this->db->where('Id_Nacionalidad',$Id_Nacionalidad);
		$resultado = $this->db->get();
		$this->db->close();
		return $resultado->result(); // despues del return solo es codigo inaccesible
	}
	
	function MostrarTodo(){
		$this->db->select('*'); //regresa todos los campos
		$this->db->from('nacionalidades');
		$resultado = $this->db->get();
		$this->db->close();
		return $resultado->result(); // despues del return solo es codigo inaccesible
	}
	
	function Eliminar($Id_Nacionalidad){		
		$this->db->where('Id_Nacionalidad',$Id_Nacionalidad);
		$this->db->delete('nacionalidades');
		$this->db->close();
	}
}
?>