<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.0/jquery.min.js"></script>
<script src="<?php echo base_url("assets/js/jquery.formatCurrency-1.4.0.min.js")?>" type="text/javascript"></script>
<script> 
	$(document).ready(function(){
		$('.moneda').formatCurrency();
		$('.moneda').change(function(){
			$(this).formatCurrency();
		});
		$('form').submit(function(){
			$('.moneda').toNumber();
		});
	});	
</script>
<?php echo form_open("admin/AgregarVino_POST") ?>
<table class='formulario'>
	<tr>
		<td>
			<span>Idioma</span>
			<select name='Idioma'>
				<option value="ES">ESPAÑOL</option>
				<option value="EN">INGLÉS</option>
			</select>
		</td>
	</tr>
	<tr>
		<td>
			<span>Nacionalidad</span>
			<select name='Id_Nacionalidad'>
				<?php 
				foreach($Nacionalidades as $row){
					echo "<option value='".$row->Id_Nacionalidad."'>".$row->Nombre."</option>";
				}
				?>
			</select>
		</td>
	</tr>
	<tr>
		<td>
			<span>Categoria</span>
			<select name='Id_Tipo_De_Vino'>
				<option value='0'></option>
				<?php 
				foreach($TipoVino as $row){
					echo "<option value='".$row->Id_Tipo_De_Vino."'>".$row->Descripcion."</option>";
				}
				?>
			</select>
		</td>
	</tr>
	<tr>
		<td>
			<span>Precio</span>
			<input type="text" name="Precio" class="moneda" />
		</td>
	</tr>
	<tr>
		<td>
			<label>Descripción</label>
			<input type="text" name="Descripcion" />
		</td>
	</tr>
	<tr>
		<td>
			<input type="submit" value="Guardar" />
		</td>
	</tr>
</table>
</form>