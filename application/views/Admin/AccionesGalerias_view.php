<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.0/jquery.min.js"></script>
<link rel="stylesheet" href="<?php echo base_url('assets/css/font-awesome.min.css')?>" />
<script src="<?php echo base_url("assets/js/jquery.tablesorter.js")?>"></script>
<script src="<?php echo base_url("assets/js/jquery.tablesorter.pager.js")?>"></script>
<script src="<?php echo base_url("assets/js/jquery.tablesorter.widgets.js")?>"></script>
<script type="text/javascript">
	$(document).ready(function(){
		$("#TablaGalerias").load('<?php echo site_url("admin/TablaGalerias")?>',function(){
			tableFilter('#TBLGalerias','#pager');
		});
		
	});
	
	function tableFilter(tabla,paginador){
		$(tabla).tablesorter({
			widgets: ["zebra", "filter"],
			widgetOptions : {
			      filter_childRows : false,
			      filter_columnFilters : true,
			      filter_cssFilter : 'tablesorter-filter',
			      filter_filteredRow   : 'filtered',
			      filter_formatter : null,
			      filter_functions : null,
			      filter_hideFilters : false, // true, (see note in the options section above)
			      filter_ignoreCase : true,
			      filter_liveSearch : true,
			      filter_reset : 'button.reset',
			      filter_searchDelay : 300,
			      filter_serversideFiltering: false,
			      filter_startsWith : false,
			      filter_useParsedData : false
				}
			}).tablesorterPager({container: $(paginador)});
			
		$(".eliminar").click(function(event){
			event.preventDefault();
			var elemento = $(this);
			if( confirm("¿Desea eliminar?") ){
				$.ajax({
					type: "GET",
					dataType: 'json',
					url: "<?php echo site_url("admin/EliminarGaleria") ?>/"+$(this).attr('href'),
					}).done(function(data) {
						console.log(data);
						if(data.Id_Galeria>0){
							$(elemento).parent().parent().empty();
						}
						else{
							alert("Ah ocurrido un error al eliminar");
						}
				});
			}
		});
	}
</script>
<div id="TablaGalerias"></div>
