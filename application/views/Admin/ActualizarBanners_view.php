<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.0/jquery.min.js"></script>
<script type="text/javascript" src="<?php echo base_url('AjaxFileUploader/ajaxfileupload.js') ?>"></script>
<script type="text/javascript">
	function ajaxFileUpload(nombreImagen,idFile,imgSubida)
	{
		$("#loading")
		.ajaxStart(function(){
			$(this).show();
		})
		.ajaxComplete(function(){
			$(this).hide();
		});
	
		$.ajaxFileUpload({
				url:'<?php echo base_url('AjaxFileUploader/SubeImagenBanner.php') ?>',
				secureuri:false,
				fileElementId: idFile,
				dataType: 'json',
				data:{name:'noel', id:'id',NombreImg:nombreImagen,Dir:"Banners",fileElement:idFile},
				success: function (data, status){
					if(typeof(data.error) != 'undefined'){
						if(data.error != ''){
							alert(data.error);
							$("#"+imgSubida).text("Intente de nuevo").removeClass("ConImagen").addClass("SinImagen");
						}
						else{
							$("#"+imgSubida).text("Con imagen").removeClass("SinImagen").addClass("ConImagen");
						}
					}
				},
				error: function (data, status, e){
					alert(e);
				}
		})
		return false;
	}
</script>
<center><h3>Sustituir banners</h3></center>
<table class="table table-borded table-striped" style="width:900px; margin:auto;">
	<thead>
		<tr>
			<th>Banner</th>
			<th>(Subir imagenes en formato .jpg)</th>
		</tr>
	</thead>
	<tr>
		<td>1</td>
		<td>
			<input id="fileToUpload01" type="file" size="45" name="fileToUpload01" class="input">
			<button class="button" id="buttonUpload" onclick="return ajaxFileUpload('imgGaleria01','fileToUpload01','ImgSubida01');"><i class="icon-cloud-upload icon-large"></i>&nbsp;&nbsp;&nbsp;Subir</button>
			<span id="ImgSubida01" class="SinImagen"></span>
		</td>
	</tr>
	<tr>
		<td>2</td>
		<td>
			<input id="fileToUpload02" type="file" size="45" name="fileToUpload02" class="input">
			<button class="button" id="buttonUpload" onclick="return ajaxFileUpload('imgGaleria02','fileToUpload02','ImgSubida02');"><i class="icon-cloud-upload icon-large"></i>&nbsp;&nbsp;&nbsp;Subir</button>
			<span id="ImgSubida02" class="SinImagen"></span>
		</td>
	</tr>
	<tr>
		<td>3</td>
		<td>
			<input id="fileToUpload03" type="file" size="45" name="fileToUpload03" class="input">
			<button class="button" id="buttonUpload" onclick="return ajaxFileUpload('imgGaleria03','fileToUpload03','ImgSubida03');"><i class="icon-cloud-upload icon-large"></i>&nbsp;&nbsp;&nbsp;Subir</button>
			<span id="ImgSubida03" class="SinImagen"></span>
		</td>
	</tr>
	<tr>
		<td>4</td>
		<td>
			<input id="fileToUpload04" type="file" size="45" name="fileToUpload04" class="input">
			<button class="button" id="buttonUpload" onclick="return ajaxFileUpload('imgGaleria04','fileToUpload04','ImgSubida04');"><i class="icon-cloud-upload icon-large"></i>&nbsp;&nbsp;&nbsp;Subir</button>
			<span id="ImgSubida04" class="SinImagen"></span>
		</td>
	</tr>
	<tr>
		<td>5</td>
		<td>
			<input id="fileToUpload05" type="file" size="45" name="fileToUpload05" class="input">
			<button class="button" id="buttonUpload" onclick="return ajaxFileUpload('imgGaleria05','fileToUpload05','ImgSubida05');"><i class="icon-cloud-upload icon-large"></i>&nbsp;&nbsp;&nbsp;Subir</button>
			<span id="ImgSubida05" class="SinImagen"></span>
		</td>
	</tr>
</table>