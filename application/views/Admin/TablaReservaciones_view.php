<script>
	$(document).ready(function(){
		$(".eliminar").click(function(event){
			event.preventDefault();
			if( confirm("¿Desea eliminar?") ){
				$.ajax({
					type: "GET",
					url: "<?php echo site_url("admin/EliminarVino") ?>/"+$(this).attr('href'),
					}).done(function() {
				});
				$(this).parent().parent().empty();
			}
		});
	});
</script>
<br />
<table class="table table-striped table-borded tblResultados" id="TBLReservaciones">
	<thead>
		<tr>
			<th>Nombre</th>
			<th>Fecha de reservación</th>
			<th>Personas</th>
			<th class="tdAcciones filter-false">Acciones</th>
		</tr>
	</thead>
<?php
if(isset($Reservaciones)){
	foreach ($Reservaciones as $row){
		echo '<tr>';
		echo '<td>'.$row->Nombre.' '.$row->Apellidos.'</td>';
		echo '<td>'.$row->Fecha_Res.'</td>';
		echo '<td>'.$row->Num_Personas.'</td>';
		echo '<td class="tdAcciones"><a href="'.site_url("admin/ActualizarReservacion")."/".$row->Id_Reservacion.'">Actualizar</a></td>';
		echo '</tr>';
	}
}
?>
</table>
<?php $this->load->view("Admin/TablePAGER_view") ?>
