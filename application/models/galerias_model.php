<?php
class Galerias_model extends CI_Model {

	function __construct() {
		 $this->load->database();
		 parent::__construct();
	}

	function Insertar($Titulo,$URL_Tapa){
		$this->load->helper('date');
		$Datos = array(
			'Titulo'	=>$Titulo,
			'URL_Tapa'	=>$URL_Tapa,
			'Fecha'		=>date('Y-m-d', now())
		);
		$this->db->insert("galerias",$Datos);
		
		if($this->db->affected_rows() > 0){
			return $this->db->insert_id();
		}
		else{
			$this->db->close();
			return 0;
		}
	}
	
	function Actualizar($Id_Galeria,$Titulo,$URL_Tapa){
		$Datos = array(
			'Titulo'	=>$Titulo,
			'URL_Tapa'	=>$URL_Tapa
		);
		$this->db->where('Id_Galeria',$Id_Galeria);
		$this->db->update("galerias",$Datos);
		
		if($this->db->affected_rows() > 0){
			$this->db->close();
			return $Id_Galeria;
		}
		else{
			$this->db->close();
			return 0;
		}
	}
	
	function ObtenerTodo(){
		$this->db->select('*'); //regresa todos los campos
		$this->db->from('galerias');
		$resultado = $this->db->get();
		$this->db->close();
		return $resultado->result(); 
	}
	
	function Obtener($Id_Galeria){
		$this->db->select('*'); //regresa todos los campos
		$this->db->from('galerias');
		$this->db->where('Id_Galeria',$Id_Galeria);
		$resultado = $this->db->get();
		$this->db->close();
		return $resultado->result(); 
	}
	
	function Eliminar($Id_Galeria){		
		$this->db->where('Id_Galeria',$Id_Galeria);
		$this->db->delete('galerias');
		if($this->db->affected_rows() > 0){
			$this->db->close();
			return $Id_Galeria;
		}
		else{
			$this->db->close();
			return 0;
		}
	}
}
?>