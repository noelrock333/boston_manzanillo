<script>
	$(document).ready(function(){
		$(".galControls").click(function(){
			var option = $(this).index()+1;
			cambioDeFondo(option);
		});
		
		var cont = 1;
		var numImgs = 4;
		var movimiento = false;
		if(movimiento==true){
			timerUno = setInterval(function(){
				if(cont<=numImgs){
					cambioDeFondo(cont);
					cont++;
				}
				else{
					cont=0;
				}
			},9500);
		}
	});
	
	function cambioDeFondo(variable){
		$('#ImagenGaleria').animate({opacity: 0}, 1000, function(){
	    	$(this).css('background-image','url(' + $("#imgGale0"+variable).attr("src") + ')');
		}).animate({opacity: 1}, 1000);
	}
</script>
<div id="galeria">
	<div id="ImagenGaleria" class="fondo01"></div>
	<div id="MensajeGaleria">
		<img src="<?php echo base_url('assets/images/fig-1.png')?>" id="fig1" />
		<div>Ven y disfruta de nuestro amplio menú de desayunos, comida y cena.<br /><br />Deliciosos cortes finos, en un ambiente sofisticado y atención de alto nivel en Manzanillo.</div>
		<img src="<?php echo base_url('assets/images/fig-2.png')?>" id="fig2" />
	</div>
</div>
<img src="<?php echo base_url('assets/images/subidas/Banners/imgGaleria01.jpg')?>" id="imgGale01" class="imgGale" />
<img src="<?php echo base_url('assets/images/subidas/Banners/imgGaleria02.jpg')?>" id="imgGale02" class="imgGale" />
<img src="<?php echo base_url('assets/images/subidas/Banners/imgGaleria03.jpg')?>" id="imgGale03" class="imgGale" />
<img src="<?php echo base_url('assets/images/subidas/Banners/imgGaleria04.jpg')?>" id="imgGale04" class="imgGale" />
<img src="<?php echo base_url('assets/images/subidas/Banners/imgGaleria05.jpg')?>" id="imgGale05" class="imgGale" />
<div id="galeriaControl">
	<div id="galControl1" class="galControls"></div>
	<div id="galControl2" class="galControls"></div>
	<div id="galControl3" class="galControls"></div>
	<div id="galControl4" class="galControls"></div>
	<div id="galControl5" class="galControls"></div>
</div>
<br />
<br />
<br />