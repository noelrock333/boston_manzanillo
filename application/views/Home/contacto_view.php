<link href="http://code.jquery.com/ui/1.10.3/themes/pepper-grinder/jquery-ui.css" rel="stylesheet" />
<script src="http://code.jquery.com/jquery-1.9.1.js" type="text/javascript"></script>
<script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js" type="text/javascript"></script>
<script src="<?php echo base_url("assets/js/jquery.meio.mask.min.js")?>" type="text/javascript"></script>
<script type="text/javascript">
	$(document).ready(function(){
		$("#Fecha_Reg").datepicker({ dateFormat: "yy-mm-dd" });
		$('input[type="text"]').setMask();
	});
</script>
<div id="ColumnaUno">
	UBICACIÓN
	<hr class="hrContacto" />
	<br />
	BOSTON RESTAURANT WINE BAR
	<br />
	<br />
	<b>Calle:</b> Boulevard Miguel de la Madrid #1847<br />
	<b>Colonia:</b> Manzanillo<br />
	Manzanillo, Colima, México<br />
	<b>CP:</b> 28218<br />
	<b>Tel:</b> 01 314 336 4072
</div>
<div id="ColumnaDos">
	RESERVACIÓN
	<hr class="hrContacto" />
	<br />
	Para hacer una reservación en Boston Restaurant Wine Bar, llene el formato a continuación <i>(reservaciones para el fin de semana tienen que ser realizadas con 72 horas de anticipación):</i>
	<br /><br />
	(información necesaria está marcada con *)
	<br /><br />
	<?php echo form_open("home/Reservacion_POST") ?>
		<table id="FormularioReservacion">
			<tr>
				<td>*Su nombre<br/><input type="text" name="Nombre" /></td>
				<td>*Apellidos<br/><input type="text" name="Apellidos" /></td>
			</tr>
			<tr>
				<td>*No. telefono<br/><input type="text" name="Telefono" /></td>
				<td>*Email<br/><input type="text" name="Email" /></td>
			</tr>
			<tr>
				<td>*Fecha de reservación<br/><input type="text" name="Fecha_Reg" id="Fecha_Reg" readonly="readonly"/></td>
				<td>*Hora de reservación<br/><input type="text" name="Hora_Reg" alt="time"/></td>
			</tr>
			<tr>
				<td>*No. de personas<br/><input type="text" name="Num_Personas" alt="integer" /></td>
				<td></td>
			</tr>
			<tr>
				<td colspan="2">*Comentarios<br/><textarea name="Comentarios" ></textarea></td>
			</tr>
			<tr>
				<td colspan="2"><input type="submit" value="Enviar" id="btnEnviar"/></td>
			</tr>
		</table>
	<?php echo form_close() ?>
</div>
<div class="clearfix"></div>

