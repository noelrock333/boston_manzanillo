<div id="pager" class="tablesorterPager">
	<form>
		<table>
			<tr>
				<td>
					<i class="icon-step-backward first"></i>
					<i class="icon-backward prev"></i>
				<td>
					<input type="text" class="pagedisplay"/>
				</td>
				<td>
					<i class="icon-forward next"></i>
					<i class="icon-step-forward last"></i>
				</td>
			</tr>
			<tr>
				<td colspan="3">
					<select class="pagesize">
						<option selected="selected"  value="10">10</option>
						<option value="20">20</option>
						<option value="30">30</option>
						<option  value="40">40</option>
					</select>
				</td>
			</tr>
		</table>
		
	</form>
</div>