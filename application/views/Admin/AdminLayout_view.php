<!DOCTYPE html>
<html lang="es">
	<head>
		<meta charset="utf-8">
		<title>Boston Manzanillo</title>
		<link rel="stylesheet" href="<?php echo base_url('assets/css/adminLayout.css')?>" />
		<link rel="stylesheet" href="<?php echo base_url('assets/css/font-awesome.min.css')?>" />
		<link rel="stylesheet" href="<?php echo base_url('assets/css/bootstrap.min.css')?>" />
		<?php 
			if(isset($Estilos)){
				foreach($Estilos as $Estilo){
					echo '<link rel="stylesheet" href="'.base_url($Estilo).'" />';
				}
			}	
		?>
	</head>
<body>
	<div id="ToolBar">
		<ul>
			<li class="menuItem selected"><a href="<?php echo site_url("admin/index")?>">INICIO</a></li>
			<li class="menuItem"><a href="#">MEN&Uacute;</a>
				<ul>
					<li><a href="<?php echo site_url('admin/AgregarAlimento')?>">AGREGAR ALIMENTO/BEBIDA</a></li>
					<li><a href="<?php echo site_url('admin/AccionesAlimentos')?>">ACCIONES ALIMENTOS/BEBIDAS</a></li>
				</ul>
			</li>
			<li class="menuItem"><a href="#">VINOS</a>
				<ul>
					<li><a href="<?php echo site_url('admin/AgregarVino')?>">AGREGAR NUEVO</a></li>
					<li><a href="<?php echo site_url('admin/AccionesVinos')?>">ACCIONES</a></li>
					<li><a href="<?php echo site_url('admin/AgregarTipoVino')?>">AGREGAR CATEGORIA</a></li>
					<li><a href="<?php echo site_url("admin/AccionesTipoVino")?>">ACCIONES CATEGORIAS</a></li>
					<li><a href="<?php echo site_url("admin/AgregarNacionalidad")?>">AGREGAR NACIONALIDAD</a></li>
					<li><a href="<?php echo site_url("admin/AccionesNacionalidades")?>">ACCIONES NACIONALIDADES</a></li>
				</ul>
			</li>
			<li class="menuItem"><a href="<?php echo site_url("admin/AgregarGaleria")?>">GALER&Iacute;A DE FOTOS</a></li>
			<li class="menuItem"><a href="#">NOTICIAS</a></li>
			<li class="menuItem"><a href="<?php echo site_url("admin/ActualizarBanners")?>">ACTUALIZAR BANNERS</a></li>
			<li class="menuItem"><a href="<?php echo site_url("admin/AccionesReservaciones")?>">RESERVACIONES</a></li>
		</ul>
		<a id="btnCerrarSesion" href="<?php echo site_url('admin/CerrarSesion') ?>">Cerrar Sesión</a>
	</div>
	<?php 
		$this->load->view($Vista);
	?>
</body>