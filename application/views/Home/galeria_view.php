<script src="<?php echo base_url("assets/js/jquery.colorbox-min.js")?>"></script>
<script>
	$(document).ready(function(){
		adaptaColorbox();
		$(window).on('resize',function(){
			adaptaColorbox();
		});
	});
	
	function adaptaColorbox(){
		if($(window).width()<800)
			$(".group1").colorbox({rel:'group1', transition:"none", width:"100%"});
		else
			$(".group1").colorbox({rel:'group1'});
	}
</script>
<br />
<br />
<br />
<?php
    foreach ($Imagenes as $row) {
		echo '<a class="group1" href="'.base_url($row->URL_Imagen).'">';
		echo '	<img class="tapaGaleria" src="'.base_url($row->URL_ImagenMin).'" />';
		echo '</a>';
    }
?>
