<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.0/jquery.min.js"></script>
<script type="text/javascript">
	$(document).ready(function(){
		$("#TablaTipoVino").load('<?php echo site_url("admin/TablaTipoVino"); ?>/TODOS');
		$("#Idioma").change(function(){
			$("#TablaTipoVino").load('<?php echo site_url("admin/TablaTipoVino"); ?>/'+$("#Idioma").val());
		});
	});
</script>
<table class="formulario">
	<tr>
		<td>
			<span> Idioma</span>
			<select id="Idioma" >
				<option value="TODOS">TODOS</option>
				<option value="ES">ESPAÑOL</option>
				<option value="EN">INGLÉS</option>
			</select>
		</td>
	</tr>
	<tr>
		<td>
			<div id="TablaTipoVino"></div>
		</td>
	</tr>
</table>