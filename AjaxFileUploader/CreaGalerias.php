<?php
	header('Content-type: application/json'); 
	$error = "";
	$msg = "";
	/* Estructura para crear carpetas */
	$Directorio = isset($_GET["Dir"]) ? $_GET["Dir"] : NULL;
	if(isset($Directorio)){
		if(is_dir("../assets/images/subidas/Galeria".$Directorio)){
			echo json_encode(array(
				'msg'	=> 'El directorio ya existe'
			));
		}
		else{
			if(mkdir("../assets/images/subidas/".$Directorio,0777)){
				mkdir("../assets/images/subidas/".$Directorio."/BigSize",0777);
				mkdir("../assets/images/subidas/".$Directorio."/SmallSize",0777);
				echo json_encode(array(
					'error'	=> false,
					'msg'	=> 'Directorio creado con exito'
				));
			}
		}
	}
	/* Estructura para crear carpetas Fin */
	else{
		echo json_encode(array(
			'msg'	=> 'No se definió el nombre del directorio'
		));
	}
?>