<?php
    
    class Noticias_model extends CI_Model {

	
	function __construct() {
		 $this->load->database();
		 parent::__construct();
	}

	function Insertar($Noticia_Completa,$Resumen_Noticia){
		$Datos = array(
		'Noticia_Completa'=>$Noticia_Completa,
		'Resumen_Noticia'=>$Resumen_Noticia,
		'Idioma'=>$Idioma
		);
		$this->db->insert("noticias",$Datos);
		$this->db->close();		
	}	

	function Actualizar($Id_Noticia,$Noticia_Completa,$Resumen_Noticia){
		$Datos = array(
		'Noticia_Completa'=> $Noticia_Completa,
		'Resumen_Noticia'=> $Resumen_Noticia,
		'Idioma'=> $Idioma	
		);
		$this->db->where('Id_Noticia',$Id_Noticia);
		$this->db->update('noticias',$Datos);
		$this->db->close();
	}
	
	function Mostrar($Id_Noticia){
		$this->db->select('*'); 
		$this->db->from('noticias');
		$this->db->where('Id_Noticia',$Id_Noticia);
		$resultado = $this->db->get();
		$this->close();
		return $resultado->result(); 
	}
	
	function Eliminar($Id_Noticia){		
		$this->db->where('Id_Noticia',$Id_Noticia);
		$this->db->delete('noticias');
		$this->db->close();
	}
	
	Function MostrarPorIdioma($Idioma){
		$this->db->Select('*');
		$this->db->from('noticias');
		$this->db->where('Idioma',$Idioma);
		$resultado = $this->db->get();
		$this->db->close();
		return $resultado->result();
	}
}
    
?>