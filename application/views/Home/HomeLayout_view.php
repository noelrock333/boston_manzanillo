<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="utf-8">
	<title>Boston Manzanillo</title>
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1"/>
	<link rel="shortcut icon" type="image/x-icon" href="<?php echo base_url('assets/images/favicon.ico')?>" />
	<link href="<?php echo base_url('assets/css/layout-movil.css')?>" rel="stylesheet" media="handheld, only screen and (max-width: 800px)" />
	<link href="<?php echo base_url('assets/css/layout.css')?>" rel="stylesheet" media="screen and (min-width: 801px)"/>
	<!--[if lt IE 9]><link href="<?php echo base_url('assets/css/layout-IE8.css')?>" rel="stylesheet" /></[endif]-->
	<?php 
		if(isset($Estilos)){
			foreach($Estilos as $Estilo){
				echo '<link rel="stylesheet" href="'.base_url($Estilo).'" />';
			}
		}	
	?>
	<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.0/jquery.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url('assets/js/css3-mediaqueries.js')?>"></script>
	<script type="text/javascript">
		$(document).on('ready',function(){
			$('.'+'<?php echo $this->router->method ?>').addClass('selected');
		});
	</script>
</head>
<body>

<div id="main">
	<div id="container">
		<div id="header">
			<div id="logoBoston"></div>
			<div id="reservaciones">
				<div id="logoReservaciones"></div>
				<label id="reservacionesTxt">RESERVACIONES</label><br />
				<label id="reservacionesNum">01 (314) 33 64072 <br/>Y 33 67285</label>
			</div>
			<a href="javascript:history.back()" id="regresar"><div>&lt; Regresar</div></a>
			<div id="nav">
				<ul>
					<li class="menuItem index"><a href="<?php echo base_url()?>">INICIO</a></li>
					<li class="menuItem Menu"><a href="#">MEN&Uacute;</a>
						<ul>
							<li><a href="<?php echo site_url("home/Menu/1")?>">DESAYUNO</a></li>
							<li><a href="<?php echo site_url("home/Menu/2")?>">COMIDA/CENA</a></li>
							<li><a href="<?php echo site_url("home/Menu/3")?>">POSTRES</a></li>
							<li><a href="<?php echo site_url("home/Menu/4")?>">BEBIDAS</a></li>
						</ul>
					</li>
					<li class="menuItem Vinos"><a href="<?php echo site_url("home/Vinos")?>">VINOS</a></li>
					<li class="menuItem Galeria Galerias"><a href="<?php echo site_url("home/Galerias")?>">GALER&Iacute;A DE FOTOS</a></li>
					<li class="menuItem Noticias"><a href="#">QUIENES SOMOS</a></li>
					<li class="menuItem Contacto"><a href="<?php echo site_url("home/Contacto")?>">CONTACTO</a></li>
				</ul>
			</div>
			<div class="clearfix"></div>
		</div>
		<div id="contenido">
			<?php
				$this->load->view($Vista);
			?>
			<div class="clearfix"></div>
		</div>
	</div>
</div>
<div id="footer">
	<div id="redesSociales">
		<span>siguenos</span><br />
		<!-- <div id="redTwitter"></div> -->
		<a href="https://www.facebook.com/BostonRestaurantWineBar"><div id="redFacebook"></div></a>
	</div>
	<div class="menuFoot">
		<div class="iconComida"></div>
		<div class="menuFootTitle">Men&uacute;</div><br/>
		<ul>
			<li class="menuIFoot"><a href="<?php echo site_url("home/Menu/1")?>">Desayunos</a></li>
			<li class="menuIFoot"><a href="<?php echo site_url("home/Menu/2")?>">Comidas/cenas</a></li>
			<li class="menuIFoot"><a href="<?php echo site_url("home/Menu/3")?>">Postres</a></li>
			<li class="menuIFoot"><a href="<?php echo site_url("home/Menu/4")?>">Bebidas</a></li>
		</ul>
	</div>
	<div class="menuFoot">
		<div class="iconVino"></div>
		<div class="menuFootTitle">Vinos</div><br/>
		<ul>
			<li class="menuIFoot"><a href="#">Desayunos</a></li>
			<li class="menuIFoot"><a href="#">Comidas/cenas</a></li>
			<li class="menuIFoot"><a href="#">Postres</a></li>
			<li class="menuIFoot"><a href="#">Bebidas</a></li>
		</ul>
	</div>
	<div class="menuFoot">
		<div class="iconInstal"></div>
		<div class="menuFootTitle">Contacto</div><br/>
		<ul>
			<li class="menuIFoot"><a href="<?php echo site_url("home/Contacto")?>"">Ubicaci&oacute;n</a></li>
			<li class="menuIFoot"><a href="<?php echo site_url("home/Contacto")?>"">Reservaci&oacute;n</a></li>
			<li class="menuIFoot"><a href="<?php echo site_url("home/Contacto")?>"">Mapa</a></li>
		</ul>
	</div>
	<div class="clearfix"></div>
	<center id="copyright">&copy; Copyright Boston Manzanillo 2013. All Rights Reserved</center><br />
</div>

</body>
</html>